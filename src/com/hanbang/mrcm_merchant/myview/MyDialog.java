package com.hanbang.mrcm_merchant.myview;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;
import com.hanbang.mrcm_merchant.R;
import com.hanbang.mrcm_merchant.annotation.SelectItem;
import com.hanbang.mrcm_merchant.timeselect.DateWheelMain;
import com.hanbang.mrcm_merchant.timeselect.WheelView;
import com.hanbang.mrcm_merchant.utils.JsonHelper;
import com.hanbang.mrcm_merchant.utils.MyLog;
import com.hanbang.mrcm_merchant.utils.SystemTools;
import com.hanbang.mrcm_merchant.utils.UiUtils;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

/*
 * 显示整个项目中要显示对话框
 * MODE: LoaddingDialog：加载对话框
 */
public class MyDialog extends Dialog {
	private ProgressBar progressBar = null;
	private TextView mbTv = null;
	private TextView biliTv = null;
	private MaxHeightListView listView = null;
	private ArrayList<? extends Object> listData = null;
	private DateWheelMain wheelMain = null;

	/*
	 * 对话框的类型 枚举 LOADDING :加载对话框
	 */
	public static enum MODE {
		LOADDING, CONFIRM, HINT, HEAD, EDITTEXT, PROGRESS, SELECT, DATE
	};

	private MODE mode = null;
	private Context context;
	// 对话框点击事件
	private OnClickCallback onClickCallback = null;

	@Override
	public void show() {
		// 判断是否加载对话框
		if (context instanceof Activity) {
			if (((Activity) context).isFinishing()) {
				return;
			}
		}
		super.show();
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		// 创建的时候回调接口
		switch (mode) {
		case CONFIRM:
			findViewById(R.id.confirm).setOnClickListener(onClickListener);
			findViewById(R.id.quxiao).setOnClickListener(onClickListener);
			break;
		case HINT:
			findViewById(R.id.confirm).setOnClickListener(onClickListener);
			break;
		case HEAD:
			findViewById(R.id.about_me_dialog_t1).setOnClickListener(
					onClickListener);
			findViewById(R.id.about_me_dialog_t2).setOnClickListener(
					onClickListener);
			findViewById(R.id.about_me_dialog_t3).setOnClickListener(
					onClickListener);
			break;
		case EDITTEXT:
			findViewById(R.id.confirm).setOnClickListener(onClickListener);
			findViewById(R.id.quxiao).setOnClickListener(onClickListener);
			break;
		case PROGRESS:
			findViewById(R.id.quxiao).setOnClickListener(onClickListener);
			mbTv = (TextView) findViewById(R.id.mb);
			biliTv = (TextView) findViewById(R.id.bili);
			progressBar = (ProgressBar) findViewById(R.id.progressBar1);
			break;
		case SELECT:
			listView = (MaxHeightListView) findViewById(R.id.listview);
			listView.setListViewHeight(SystemTools.dip2px(context, 300));
			listView.setOnItemClickListener(new OnItemClickListener() {

				@Override
				public void onItemClick(AdapterView<?> parent, View view,
						int position, long id) {
					if (onClickCallback != null && listData != null) {
						onClickCallback.onClick(MyDialog.this, position,
								listData.get(position));
					}
					dismiss();
				}
			});
			if (listData != null) {
				listView.setAdapter(new MyAdapter());
			}
			break;
		case DATE:
			findViewById(R.id.confirm).setOnClickListener(onClickListener);
			findViewById(R.id.quxiao).setOnClickListener(onClickListener);
			wheelMain = new DateWheelMain((Activity) context,
					(WheelView) findViewById(R.id.year),
					(WheelView) findViewById(R.id.month),
					(WheelView) findViewById(R.id.day),
					(WheelView) findViewById(R.id.hour),
					(WheelView) findViewById(R.id.min));
			break;
		default:
			break;
		}

		UiUtils.applyFont(getWindow().getDecorView().findViewById(
				android.R.id.content));

	}

	public MyDialog(Context context, int theme, MODE mode) {
		super(context, theme);
		this.mode = mode;
		this.context = context;
		switch (mode) {
		case LOADDING:
			// R.style.loadding_dialog
			setContentView(R.layout.loadding_dialog);
			getWindow().getAttributes().gravity = Gravity.CENTER;
			setCancelable(false);
			break;
		case CONFIRM:
			setContentView(R.layout.confirm_dialog);
			getWindow().getAttributes().gravity = Gravity.CENTER;
			break;
		case HINT:
			setContentView(R.layout.hint_dialog);
			getWindow().getAttributes().gravity = Gravity.CENTER;
			break;
		case HEAD:
			setContentView(R.layout.about_me_dialog);
			getWindow().getAttributes().gravity = Gravity.BOTTOM;
			break;
		case EDITTEXT:
			setContentView(R.layout.edittext_dialog);
			getWindow().getAttributes().gravity = Gravity.CENTER;
			break;
		case PROGRESS:
			setContentView(R.layout.progress_dialog);
			getWindow().getAttributes().gravity = Gravity.CENTER;
			setCancelable(false);
			break;
		case SELECT:
			setContentView(R.layout.select_dialog);
			getWindow().getAttributes().gravity = Gravity.CENTER;
			break;
		case DATE:
			setContentView(R.layout.datepicker_dialog);
			getWindow().getAttributes().gravity = Gravity.CENTER;
			break;
		default:
			break;
		}
	}

	public MyDialog(Context context) {
		super(context);
	}

	@Override
	public void dismiss() {
		super.dismiss();
	}

	@Override
	public void onWindowFocusChanged(boolean hasFocus) {
		super.onWindowFocusChanged(hasFocus);
		/*
		 * 加载对话框 旋转动画
		 */
		if (mode == MODE.LOADDING) {
			ImageView imageView = (ImageView) findViewById(R.id.image);
			Animation animation = AnimationUtils.loadAnimation(context,
					R.anim.loadding_dialog_round);
			imageView.startAnimation(animation);
		}
	}

	/*
	 * **********************对话框点击事件********************
	 */
	@Override
	public void onBackPressed() {
		super.onBackPressed();
		if (onClickCallback != null) {
			onClickCallback.onClick(MyDialog.this, -1, "dismiss");
		}

	}

	private android.view.View.OnClickListener onClickListener = new android.view.View.OnClickListener() {

		@Override
		public void onClick(View v) {

			if (onClickCallback != null) {
				switch (v.getId()) {
				case R.id.confirm:
					if (mode == MODE.DATE) {
						if (wheelMain != null) {
							onClickCallback.onClick(MyDialog.this, 2,
									wheelMain.getTime());
						}
					} else {
						TextView view = (TextView) findViewById(R.id.content);
						String c = null;
						if (view != null) {
							c = view.getText().toString();
						}
						onClickCallback.onClick(MyDialog.this, 2, c);
					}

					dismiss();
					break;
				case R.id.quxiao:
					onClickCallback.onClick(MyDialog.this, 1, "quxiao");
					dismiss();
					break;
				case R.id.about_me_dialog_t1:
					onClickCallback.onClick(MyDialog.this, 1, null);
					dismiss();
					break;
				case R.id.about_me_dialog_t2:
					onClickCallback.onClick(MyDialog.this, 2, null);
					dismiss();
					break;
				case R.id.about_me_dialog_t3:
					onClickCallback.onClick(MyDialog.this, 3, null);
					dismiss();
					break;

				default:
					break;
				}
			} else {
				dismiss();
			}

		}
	};

	/*
	 * 接口
	 */

	public interface OnClickCallback {
		public void onClick(MyDialog dialog, Object number, Object content);
	}

	public void setOnClickCallback(OnClickCallback callback) {
		onClickCallback = callback;
	}

	/*
	 * **********************进度条对话框设置****************
	 */
	public MyDialog setProgress(int c) {
		if (progressBar != null && biliTv != null) {
			progressBar.setProgress(c);
			biliTv.setText(c + "%");
		}
		return this;
	}

	public MyDialog setMb(Float count, Float current) {
		if (progressBar != null && mbTv != null) {
			mbTv.setText(current + "M/" + count + "M");
		}
		return this;
	}

	/*
	 * **********************选择对话框设置****************
	 */

	public void setSelectData(ArrayList<? extends Object> data) {
		listData = data;
		if (listView != null) {
			listView.setAdapter(new MyAdapter());
		}

	}

	/*
	 * **********************对话框设置****************
	 */

	/*
	 * 设置内容
	 */
	public MyDialog setContent(String content) {
		TextView c = ((TextView) findViewById(R.id.content));
		if (c != null && content != null) {
			c.setText(content);
		}
		if (c != null && content == null) {
			c.setVisibility(View.GONE);
		}
		return this;
	}

	/*
	 * 设置标题
	 */
	public MyDialog setTitle(String title) {
		TextView tt = (TextView) findViewById(R.id.title);
		if (tt != null && title != null) {
			tt.setText(title);
		}
		if (tt != null && title == null) {
			tt.setVisibility(View.GONE);
		}
		return this;
	}

	/*
	 * 设置标题 肢体颜色
	 */
	public MyDialog setTitleColor(int color) {
		TextView tt = (TextView) findViewById(R.id.title);
		if (tt != null) {
			tt.setTextColor(color);
		}
		return this;
	}

	/*
	 * 设置确定按钮 文字 默认确定
	 */
	public MyDialog setConfirm(String confirm) {
		TextView queding = ((TextView) findViewById(R.id.confirm));
		if (queding != null && confirm != null) {
			queding.setText(confirm);
		}
		return this;
	}

	/*
	 * 设置取消按钮 文字 默认取消
	 */
	public MyDialog setQuxiao(String confirm) {
		TextView quxiao = ((TextView) findViewById(R.id.quxiao));
		if (quxiao != null && confirm != null) {
			quxiao.setText(confirm);
		}
		return this;
	}

	/*
	 * 设置确定按钮 文字颜色
	 */
	public MyDialog setConfirmColor(int color) {
		TextView queding = ((TextView) findViewById(R.id.confirm));
		if (queding != null && color != 0) {
			queding.setTextColor(color);
		}
		return this;
	}

	/*
	 * 设置取消按钮 文字 默认取消
	 */
	public MyDialog setQuxiaoColor(int color) {
		TextView quxiao = ((TextView) findViewById(R.id.quxiao));
		if (quxiao != null && color != 0) {
			quxiao.setTextColor(color);
		}
		return this;
	}

	class MyAdapter extends BaseAdapter {

		@Override
		public int getCount() {
			if (listData == null) {
				return 0;
			} else {
				return listData.size();
			}
		}

		@Override
		public Object getItem(int position) {
			if (listData == null) {
				return null;
			} else {
				return listData.get(position);
			}

		}

		@Override
		public long getItemId(int position) {
			return 0;
		}

		@SuppressLint("InflateParams")
		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			if (convertView == null) {
				convertView = getLayoutInflater().inflate(
						R.layout.select_dialog_item, null);
				UiUtils.applyFont(convertView);
			}

			TextView textview = (TextView) convertView
					.findViewById(R.id.textview);
			textview.setText(getSelectDialogItemName(listData.get(position)));
			return convertView;
		}

	}

	private String getSelectDialogItemName(Object obj) {
		Class<? extends Object> clazz = obj.getClass();
		// 取出bean里的所有方法
		Method[] methods = clazz.getDeclaredMethods();
		Field[] fields = clazz.getDeclaredFields();
		for (Field f : fields) {
			// 判断这个字段是否 是需要的SelectItem
			if (f.getAnnotation(SelectItem.class) == null) {
				continue;
			}
			String getMetodName = JsonHelper
					.parseMethodName(f.getName(), "get");
			if (!JsonHelper.haveMethod(methods, getMetodName)) {
				continue;
			}
			try {
				MyLog.e("getSelectDialogItemName", getMetodName);
				Method fieldMethod = clazz.getMethod(getMetodName,
						new Class[] {});
				MyLog.e("getSelectDialogItemName",
						(String) fieldMethod.invoke(obj));
				return (String) fieldMethod.invoke(obj);

			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return "";
	}
}
