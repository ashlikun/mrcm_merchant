package com.hanbang.mrcm_merchant.myview;

import java.util.Timer;
import java.util.TimerTask;
import android.content.Context;
import android.os.Handler;
import android.util.AttributeSet;
import android.widget.TextView;

public class CountdownTextView extends TextView {

	private long time = 0;
	private boolean isRun = false;
	private long day = 0;
	private long hours = 0;
	private long minutes = 0;
	private long second = 0;
	private Timer timer = new Timer();
	private TimerTask task = new TimerTask() {

		@Override
		public void run() {
			if (isRun) {
				if (time >= 0) {
					time--;
					handler.sendEmptyMessage(158);
				} else {
					isRun = false;
					timer.cancel();
				}
			}
		}
	};
	private Handler handler = new Handler() {

		public void handleMessage(android.os.Message msg) {
			super.handleMessage(msg);
			if (msg.what == 158) {
				setText(getTimeToString());
			}

		};

	};

	public CountdownTextView(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public CountdownTextView(Context context) {
		this(context, null);
	}

	@Override
	protected void onDetachedFromWindow() {
		super.onDetachedFromWindow();
		isRun = false;
		timer.cancel();
	}

	@Override
	protected void onAttachedToWindow() {
		super.onAttachedToWindow();
		timer.schedule(task, 1000, 1000);
	}

	public long getTime() {
		return time;
	}

	public void setTime(long t) {
		if (t < 0) {
			t = 0;
		}
		this.time = t;
		isRun = true;
	}

	public boolean isRun() {
		return isRun;
	}

	public void setRun(boolean isRun) {
		this.isRun = isRun;
	}

	private String getTimeToString() {
		StringBuilder stringBuilder = new StringBuilder();
		if (time >= 0) {
			stringBuilder.append("剩余 ");
			day = time / (24 * 60 * 60);
			hours = (time % (24 * 60 * 60)) / (60 * 60);
			minutes = ((time % (24 * 60 * 60)) % (60 * 60)) / 60;
			second = ((time % (24 * 60 * 60)) % (60 * 60)) % 60;
			stringBuilder.append(day);
			stringBuilder.append("天  ");
			stringBuilder.append(hours);
			stringBuilder.append("时");
			stringBuilder.append(minutes);
			stringBuilder.append("分");
			stringBuilder.append(second);
			stringBuilder.append("秒");
		} else {
			stringBuilder.append("已结束");
		}
		return stringBuilder.toString();

	}
}
