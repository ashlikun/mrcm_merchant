package com.hanbang.mrcm_merchant.application;

import java.util.Stack;

import android.app.Activity;

public class ActivityManager {
	private static Stack<Activity> activityStack;
	private static ActivityManager instance;

	private ActivityManager() {
	}

	public static ActivityManager getInstance() {
		if (instance == null) {
			instance = new ActivityManager();
		}
		return instance;
	}

	// 退出Activity
	public void popActivity(Activity activity) {
		if (activity != null) {
			// 在从自定义集合中取出当前Activity时，也进行了Activity的关闭操作
			activityStack.remove(activity);
			activity.finish();
			activity = null;
		}
	}

	// 退出Activity
	public void popActivity(@SuppressWarnings("rawtypes") Class activity) {
		if (activity != null) {
			// 在从自定义集合中取出当前Activity时，也进行了Activity的关闭操作
			for (Activity a : activityStack) {
				if (activity.equals(a.getClass())) {
					activityStack.remove(a);
					a.finish();
					a = null;
				}
			}

		}
	}

	// 获得当前栈顶Activity
	public Activity currentActivity() {
		Activity activity = null;
		if (!activityStack.empty())
			activity = activityStack.lastElement();
		return activity;
	}

	// 将当前Activity推入栈中
	public void pushActivity(Activity activity) {
		if (activityStack == null) {
			activityStack = new Stack<Activity>();
		}
		activityStack.add(activity);
	}

	// 将当前Activity 移除栈中
	public void exitActivity(Activity activity) {
		if (activityStack == null) {
			activityStack = new Stack<Activity>();
		}
		activityStack.remove(activity);
	}

	// 退出栈中所有Activity
	public void exitAllActivity() {
		while (true) {
			Activity activity = currentActivity();
			if (activity == null) {
				break;
			}
			popActivity(activity);
		}
	}

}
