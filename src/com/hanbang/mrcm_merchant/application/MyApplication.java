package com.hanbang.mrcm_merchant.application;

import java.io.File;

import com.hanbang.mrcm_merchant.bean.UserInforData;
import com.hanbang.mrcm_merchant.utils.MyLog;
import com.lidroid.xutils.HttpUtils;
import android.app.Application;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Environment;

public class MyApplication extends Application {
	public NetWorkType netWorkType = NetWorkType.NULL;
	public Typeface typeface = null;
	public HttpUtils httpUtils = null;
	public static String bitmapCachePath = "";
	public static String cameraPath = "";
	public static String picFilepath = "";
	public static String otherPath = "";
	public UserInforData userData = null;

	public enum NetWorkType {
		WIFI, ETHERNET, MOBILE, NULL
	};

	@Override
	public void onCreate() {
		super.onCreate();
		IntentFilter filter = new IntentFilter();
		filter.addAction(ConnectivityManager.CONNECTIVITY_ACTION);
		registerReceiver(receiver, filter);
		typeface = Typeface.createFromAsset(getAssets(), "fzlt.ttf");
		httpUtils = new HttpUtils(5 * 1000);
		httpUtils.configDefaultHttpCacheExpiry(0);
		MyLog.e("ssssssssssssssss", "123");
		bitmapCachePath = Environment.getExternalStorageDirectory().getPath()
				+ "/mrcmMerchant/imagecach";
		cameraPath = Environment.getExternalStorageDirectory().getPath()
				+ "/mrcmMerchant/cameraImage";
		picFilepath = Environment.getExternalStorageDirectory().getPath()
				+ "/mrcmMerchant/cameraImage";
		otherPath = Environment.getExternalStorageDirectory().getPath()
				+ "/mrcmMerchant/myheader";
		File file = new File(bitmapCachePath);
		if (file.exists() || file.mkdirs())
			;
		file = new File(cameraPath);
		if (file.exists() || file.mkdirs())
			;
		file = new File(picFilepath);
		if (file.exists() || file.mkdirs())
			;
		file = new File(otherPath);
		if (file.exists() || file.mkdirs())
			;
	}

	BroadcastReceiver receiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			String action = intent.getAction();
			// 判断 这个动作是否是网络连通�?�发过来�?
			if (ConnectivityManager.CONNECTIVITY_ACTION.equals(action)) {
				// 手机连接网络管理类
				ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
				// 从手机连通�?�里面获取网络状态信息?
				NetworkInfo netInfo = cm.getActiveNetworkInfo();
				// 判断网路状态类型?
				if (netInfo != null && netInfo.isAvailable()) {
					if (netInfo.getType() == ConnectivityManager.TYPE_WIFI) {
						// WiFi网络
						netWorkType = NetWorkType.WIFI;

					} else if (netInfo.getType() == ConnectivityManager.TYPE_ETHERNET) {
						// 有线网络
						netWorkType = NetWorkType.ETHERNET;

					} else if (netInfo.getType() == ConnectivityManager.TYPE_MOBILE) {
						// 3g网络
						netWorkType = NetWorkType.MOBILE;
					}
				} else {
					// 无网络?
					netWorkType = NetWorkType.NULL;
				}
			}

		}
	};

	public void onTerminate() {
		unregisterReceiver(receiver);
		super.onTerminate();
	};

	@Override
	public void onLowMemory() {
		unregisterReceiver(receiver);
		super.onLowMemory();
	}
}
