package com.hanbang.mrcm_merchant.announce;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import org.json.JSONException;
import org.json.JSONObject;
import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;
import android.widget.TextView;
import com.baidu.location.BDLocation;
import com.baidu.location.BDLocationListener;
import com.baidu.location.LocationClient;
import com.baidu.location.LocationClientOption;
import com.baidu.location.LocationClientOption.LocationMode;
import com.hanbang.mrcm_merchant.R;
import com.hanbang.mrcm_merchant.aboutme.AboutMeActivity;
import com.hanbang.mrcm_merchant.application.MyApplication;
import com.hanbang.mrcm_merchant.base.MainBaseActivity;
import com.hanbang.mrcm_merchant.bean.ActivityTypeData;
import com.hanbang.mrcm_merchant.bean.ImagesData;
import com.hanbang.mrcm_merchant.bean.TimuData;
import com.hanbang.mrcm_merchant.convert.ConvertActivity;
import com.hanbang.mrcm_merchant.myview.GridViewForScrollView;
import com.hanbang.mrcm_merchant.myview.MyDialog;
import com.hanbang.mrcm_merchant.myview.ScaleImageView;
import com.hanbang.mrcm_merchant.myview.MyDialog.OnClickCallback;
import com.hanbang.mrcm_merchant.old_activity.OldActivityMeActivity;
import com.hanbang.mrcm_merchant.photo.ClipLayout;
import com.hanbang.mrcm_merchant.photo.ClipPhotoActivity;
import com.hanbang.mrcm_merchant.utils.EditHelper;
import com.hanbang.mrcm_merchant.utils.HttpInterfaces;
import com.hanbang.mrcm_merchant.utils.JsonHelper;
import com.hanbang.mrcm_merchant.utils.MyLog;
import com.hanbang.mrcm_merchant.utils.MyRequestCallBack;
import com.hanbang.mrcm_merchant.utils.MyToast;
import com.hanbang.mrcm_merchant.utils.SystemTools;
import com.lidroid.xutils.http.RequestParams;
import com.lidroid.xutils.http.ResponseInfo;
import com.lidroid.xutils.http.client.HttpRequest.HttpMethod;

public class AnnounceActivity extends MainBaseActivity {

	public LocationClient mLocationClient = null;
	public BDLocationListener myListener = new MyLocationListener();
	private TextView riqi = null;
	private TextView fangshi = null;
	private TextView leibie = null;
	private EditHelper editHelper = new EditHelper(this);
	private GridViewForScrollView gridView = null;
	private ArrayList<ImagesData> imgData = new ArrayList<ImagesData>();
	private MyAdapter adapter = null;
	// 拍完照存放的名称
	private String camera = "capture.jpg";

	private LinearLayout sharedCountll = null, tiMuLL = null;
	private EditText sharedCount = null;
	private TextView tiMu = null;
	private boolean again = false;

	/*
	 * 活动分类的集合
	 */
	private ArrayList<ActivityTypeData> activityTypeList = new ArrayList<ActivityTypeData>();
	/*
	 * 领取方式的集合
	 */
	private ArrayList<ActivityTypeData> claimTypeList = new ArrayList<ActivityTypeData>();

	/*
	 * 题目的集合
	 */
	private ArrayList<TimuData> timuList = new ArrayList<TimuData>();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.announce);

		registerReceiver(broadcastReceiver, new IntentFilter(
				OldActivityMeActivity.AGAIN_BROADCAST));

		httpUtils.configSoTimeout(1000 * 25);
		initView();
		setOnClickListener();
		initLocation();
		ImagesData d = new ImagesData();
		d.setSdPath("end");
		imgData.add(d);
		adapter.notifyDataSetChanged();
		getActivityType();
		getClaimType();
		getTimu();

	}

	private void initLocation() {
		mLocationClient = new LocationClient(getApplicationContext()); // 声明LocationClient类
		mLocationClient.registerLocationListener(myListener); // 注册监听函数
		LocationClientOption option = new LocationClientOption();
		option.setLocationMode(LocationMode.Hight_Accuracy);// 可选，默认高精度，设置定位模式，高精度，低功耗，仅设备
		option.setCoorType("bd09ll");// 可选，默认gcj02，设置返回的定位结果坐标系
		int span = 1000;
		option.setScanSpan(span);// 可选，默认0，即仅定位一次，设置发起定位请求的间隔需要大于等于1000ms才是有效的
		option.setIsNeedAddress(true);// 可选，设置是否需要地址信息，默认不需要
		option.setOpenGps(true);// 可选，默认false,设置是否使用gps
		option.setLocationNotify(true);// 可选，默认false，设置是否当gps有效时按照1S1次频率输出GPS结果
		option.setIsNeedLocationDescribe(true);// 可选，默认false，设置是否需要位置语义化结果，可以在BDLocation.getLocationDescribe里得到，结果类似于“在北京天安门附近”
		option.setIsNeedLocationPoiList(true);// 可选，默认false，设置是否需要POI结果，可以在BDLocation.getPoiList里得到
		option.setIgnoreKillProcess(false);// 可选，默认false，定位SDK内部是一个SERVICE，并放到了独立进程，设置是否在stop的时候杀死这个进程，默认杀死
		option.SetIgnoreCacheException(false);// 可选，默认false，设置是否收集CRASH信息，默认收集
		option.setEnableSimulateGps(false);// 可选，默认false，设置是否需要过滤gps仿真结果，默认需要
		mLocationClient.setLocOption(option);
	}

	/*
	 * 设置点击事件
	 */
	private void setOnClickListener() {
		ViewGroup v1 = (ViewGroup) findViewById(R.id.bottom_bar_ll1);
		ViewGroup v2 = (ViewGroup) findViewById(R.id.bottom_bar_ll2);
		ViewGroup v3 = (ViewGroup) findViewById(R.id.bottom_bar_ll3);
		setBottonButton(v2);
		v1.setOnClickListener(onClickListener);
		v3.setOnClickListener(onClickListener);
		riqi.setOnClickListener(onClickListener);
		fangshi.setOnClickListener(onClickListener);
		leibie.setOnClickListener(onClickListener);

		findViewById(R.id.ok).setOnClickListener(onClickListener);
		findViewById(R.id.location).setOnClickListener(onClickListener);
		findViewById(R.id.timu).setOnClickListener(onClickListener);
		findViewById(R.id.shared_count).setOnClickListener(onClickListener);
	}

	private OnClickListener onClickListener = new OnClickListener() {
		MyDialog dialog = null;

		@Override
		public void onClick(View v) {
			Intent intent = new Intent();
			switch (v.getId()) {
			case R.id.bottom_bar_ll1:
				intent.setClass(AnnounceActivity.this, ConvertActivity.class);
				intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
				startActivity(intent);
				break;
			case R.id.bottom_bar_ll3:
				intent.setClass(AnnounceActivity.this,
						OldActivityMeActivity.class);
				intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
				startActivity(intent);
				break;
			case R.id.delete:
				imgData.remove(v.getTag());
				adapter.notifyDataSetChanged();
				break;

			case R.id.ok:
				if (isLogin(true) && editHelper.check()) {
					releaseActivity();
				}
				break;
			case R.id.location:
				if (mLocationClient != null) {
					mLocationClient.start();
				}

				break;
			case R.id.timu:
				dialog = new MyDialog(AnnounceActivity.this,
						R.style.base_dialog, MyDialog.MODE.SELECT);
				dialog.setSelectData(timuList);
				dialog.setTitle("选择题目");
				dialog.setOnClickCallback(new OnClickCallback() {

					@Override
					public void onClick(MyDialog dialog, Object number,
							Object content) {
						if ((Integer) number != -1) {
							tiMu.setText(((TimuData) content).getTitle());
							tiMu.setTag(timuList.get((Integer) number).getId());
						}
					}
				});

				dialog.show();
				break;

			case R.id.leibie:
				dialog = new MyDialog(AnnounceActivity.this,
						R.style.base_dialog, MyDialog.MODE.SELECT);
				dialog.setSelectData(activityTypeList);
				dialog.setTitle("选择活动分类");
				dialog.setOnClickCallback(new OnClickCallback() {

					@Override
					public void onClick(MyDialog dialog, Object number,
							Object content) {
						if ((Integer) number != -1) {
							leibie.setText(((ActivityTypeData) content)
									.getTitle());
							leibie.setTag(activityTypeList
									.get((Integer) number).getId());

						}
					}
				});

				dialog.show();
				break;

			case R.id.fangshi:
				dialog = new MyDialog(AnnounceActivity.this,
						R.style.base_dialog, MyDialog.MODE.SELECT);
				dialog.setSelectData(claimTypeList);
				dialog.setTitle("选择领取方式");
				dialog.setOnClickCallback(new OnClickCallback() {

					@Override
					public void onClick(MyDialog dialog, Object number,
							Object content) {
						if ((Integer) number != -1) {
							fangshi.setText(((ActivityTypeData) content)
									.getTitle());
							fangshi.setTag(claimTypeList.get((Integer) number)
									.getType());
							selectTypeUi(claimTypeList.get((Integer) number)
									.getType());
						}
					}
				});

				dialog.show();

				break;

			case R.id.riqi:

				dialog = new MyDialog(AnnounceActivity.this,
						R.style.base_dialog, MyDialog.MODE.DATE);
				dialog.setOnClickCallback(new OnClickCallback() {

					@SuppressLint("SimpleDateFormat")
					@Override
					public void onClick(MyDialog dialog, Object number,
							Object content) {
						if ((Integer) number == 2) {
							SimpleDateFormat myFormatter = new SimpleDateFormat(
									"yyyy-MM-dd HH:mm");
							Calendar calendar = (Calendar) content;
							String res = myFormatter.format(calendar.getTime());
							riqi.setText(res);
						}
					}
				});
				dialog.setTitle("日期选择");

				dialog.show();
				break;
			default:
				break;
			}
		}
	};

	private void selectTypeUi(int type) {
		switch (type) {
		case 0:
			tiMuLL.setVisibility(View.GONE);
			sharedCountll.setVisibility(View.GONE);
			break;
		case 1:
			tiMuLL.setVisibility(View.GONE);
			sharedCountll.setVisibility(View.VISIBLE);
			break;
		case 2:
			tiMuLL.setVisibility(View.VISIBLE);
			sharedCountll.setVisibility(View.GONE);
			break;

		default:
			break;
		}

	}

	/*
	 * 初始化view
	 */
	private void initView() {
		// 设置顶部的标题
		setTitle(findViewById(R.id.top_bar_title),
				getString(R.string.botton_tab2));
		riqi = (TextView) findViewById(R.id.riqi);
		fangshi = (TextView) findViewById(R.id.fangshi);
		leibie = (TextView) findViewById(R.id.leibie);
		gridView = (GridViewForScrollView) findViewById(R.id.gridview);
		adapter = new MyAdapter();
		gridView.setAdapter(adapter);

		editHelper.setEditText((EditText) findViewById(R.id.title),
				(TextView) findViewById(R.id.dianming), leibie, fangshi,
				(TextView) findViewById(R.id.number),
				(TextView) findViewById(R.id.address), riqi,
				(TextView) findViewById(R.id.phone),
				(TextView) findViewById(R.id.jieshao));

		editHelper.setHintMinNumber(1, 1, 1, 1, 1, 1, 1, 11, 1);
		editHelper.setHintText("请输入标题", "请输入店名", "请选择活动分类", "请选择领取方式", "请输入数量",
				"请输入地址", "请输入结束日期", "请正确输入联系电话", "请输入活动介绍");
		sharedCountll = (LinearLayout) findViewById(R.id.shared_count_ll);
		sharedCount = (EditText) findViewById(R.id.shared_count);
		tiMuLL = (LinearLayout) findViewById(R.id.timu_ll);
		tiMu = (TextView) findViewById(R.id.timu);

	}

	private class MyAdapter extends BaseAdapter {

		@Override
		public int getCount() {
			return imgData.size();
		}

		@Override
		public Object getItem(int position) {
			// TODO Auto-generated method stub
			return imgData.get(position);
		}

		@Override
		public long getItemId(int position) {
			return 0;
		}

		@SuppressLint("InflateParams")
		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			ViewHolder holder = null;
			if (convertView == null) {
				convertView = getLayoutInflater().inflate(
						R.layout.pic_gridview_item, null);
				holder = new ViewHolder();
				holder.delete = (ImageView) convertView
						.findViewById(R.id.delete);
				holder.imageView = (ScaleImageView) convertView
						.findViewById(R.id.image);
				convertView.setTag(holder);
			} else {
				holder = (ViewHolder) convertView.getTag();
			}
			if (imgData.get(position).getSdPath() != null
					&& imgData.get(position).getSdPath().equals("end")) {
				holder.delete.setVisibility(View.GONE);
				holder.imageView.setImageResource(R.drawable.add_image);
				holder.imageView.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						MyDialog dialog = new MyDialog(AnnounceActivity.this,
								R.style.base_dialog, MyDialog.MODE.HEAD);
						dialog.setOnClickCallback(new OnClickCallback() {

							@Override
							public void onClick(MyDialog dialog, Object number,
									Object content) {
								// 拍照
								if ((Integer) number == 1) {
									File fDir = new File(
											MyApplication.picFilepath);
									if (fDir.exists() || fDir.mkdirs()) {
										File cameraFile = new File(fDir, camera);
										if (!cameraFile.exists()) {
											try {
												cameraFile.createNewFile();
											} catch (IOException e) {
												e.printStackTrace();
											}
										}
										Intent intent = new Intent(
												MediaStore.ACTION_IMAGE_CAPTURE);
										intent.putExtra(
												MediaStore.EXTRA_OUTPUT,
												Uri.fromFile(cameraFile));
										startActivityForResult(intent, 998);
									}
									// 相册选择
								} else if ((Integer) number == 2) {
									// 获取相册
									Intent intent = new Intent();
									intent.setType("image/*");
									intent.setAction(Intent.ACTION_GET_CONTENT);
									intent.putExtra("return-data", true);
									startActivityForResult(intent, 999);
								}

							}
						});
						dialog.show();

					}
				});
			} else {
				String imageUrl = "";
				if (imgData.get(position).getSdPath() != null
						&& !imgData.get(position).getSdPath().equals("")) {
					imageUrl = imgData.get(position).getSdPath();
				} else {
					imageUrl = SystemTools.getHttpFileUrl(imgData.get(position)
							.getOriginal_path());
				}
				bitmapUtils.display(holder.imageView, imageUrl);
				holder.delete.setVisibility(View.VISIBLE);
				holder.delete.setTag(imgData.get(position));
				holder.delete.setOnClickListener(onClickListener);
			}
			return convertView;
		}

	}

	private class ViewHolder {
		ScaleImageView imageView = null;
		ImageView delete = null;
	}

	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		/*
		 * 相册 resultCode: 正常返回-1 用户后退返回0
		 */

		if (requestCode == 999 && resultCode == -1) {
			Intent intent = new Intent(AnnounceActivity.this,
					ClipPhotoActivity.class);
			Uri uri = data.getData();
			intent.putExtra("path",
					SystemTools.getPath(AnnounceActivity.this, uri));
			configIntent(intent);
			startActivityForResult(intent, 1000);
		}
		/*
		 * 相片裁剪界面发过来的
		 */
		if (requestCode == 1000 && resultCode == 1000) {
			String p = data.getStringExtra("path");
			ImagesData imagesData = new ImagesData();
			if (imgData.size() > 0) {
				imagesData.setSdPath(p);
				imgData.add(imgData.size() - 1, imagesData);
			} else {
				imgData.add(imgData.size(), imagesData);
			}
			adapter.notifyDataSetChanged();

		}
		// 拍照返回的数据
		if (requestCode == 998 && resultCode == -1) {
			Intent intent = new Intent(AnnounceActivity.this,
					ClipPhotoActivity.class);
			File fDir = new File(MyApplication.picFilepath);
			if (fDir.exists() || fDir.mkdirs()) {
				File cameraFile = new File(fDir, camera);
				if (cameraFile.exists()) {
					intent.putExtra("path", cameraFile.getPath());
					configIntent(intent);
					startActivityForResult(intent, 1000);
				} else {
					MyToast.show(AnnounceActivity.this, "获取照片失败！",
							Toast.LENGTH_SHORT);
				}
			} else {
				MyToast.show(AnnounceActivity.this, "获取照片失败！",
						Toast.LENGTH_SHORT);
			}
		}
		super.onActivityResult(requestCode, resultCode, data);
	};

	/*
	 * 
	 * http://mrcm.hbung.com/tools/good_list.ashx?action=release_activity&id=3&
	 * user_name
	 * =yangyu&password=yangyu&type=2&activitytitle=锤子&tradename=大宇手机店&quantity
	 * =120
	 * &address=苏州市姑苏区&endtime=2015-10-30&mobile=17051196428&activityintroduction
	 * =好一个锤子手机&activityimg=123.jpg--发布活动
	 */
	private void releaseActivity() {
		if (!isLogin(true)) {
			return;
		}
		RequestParams params = getReleaseActivityParams();

		if (params != null) {

			httpUtils.send(HttpMethod.POST, HttpInterfaces.GOOD_LIST_POST,
					params, new MyRequestCallBack<String>(this,
							getString(R.string.loadding)) {
						@Override
						public void onSuccess(ResponseInfo<String> responseInfo) {
							super.onSuccess(responseInfo);

							try {
								JSONObject json = new JSONObject(
										responseInfo.result);
								int status = json.getInt("status");
								if (status == 1) {
									MyToast.show(AnnounceActivity.this,
											json.getString("msg"),
											Toast.LENGTH_SHORT);
									editHelper.clear();
									imgData.clear();
									ImagesData d = new ImagesData();
									d.setSdPath("end");
									imgData.add(d);
									adapter.notifyDataSetChanged();
									again = false;
								} else if (status == 0) {
									MyToast.show(AnnounceActivity.this,
											json.getString("msg"),
											Toast.LENGTH_SHORT);
								} else if (status == -100) {
									passwordIsError(false);
								}
							} catch (JSONException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}

						}
					});
		}

	}

	private RequestParams getReleaseActivityParams() {

		ArrayList<String> str = editHelper.getText();
		RequestParams params = new RequestParams();
		// 检查
		switch ((Integer) fangshi.getTag()) {
		case 0:

			break;
		case 1:
			// 分享领取 检查是否输入分享次数
			if (!TextUtils.isEmpty(sharedCount.getText().toString().trim())) {
				params.addBodyParameter("sharecount", sharedCount.getText()
						.toString().trim());
				if (Integer.valueOf(sharedCount.getText().toString()) <= 0) {
					MyToast.show(this, "分享次数必须大于0", Toast.LENGTH_SHORT);
					return null;
				}
			} else {
				MyToast.show(this, "请输入分享次数", Toast.LENGTH_SHORT);
				return null;
			}
			break;
		case 2:
			// 答题领取 检查是否选择答题题目
			if (!TextUtils.isEmpty(tiMu.getText().toString().trim())) {
				params.addBodyParameter("answer_id", tiMu.getTag().toString()
						.trim());
			} else {
				MyToast.show(this, "请选择答题题目", Toast.LENGTH_SHORT);
				return null;
			}
			break;

		default:
			return null;
		}

		if (again) {
			params.addBodyParameter("action", "again_activity");
			params.addBodyParameter("activity_id", String.valueOf(activity_id));
		} else {
			params.addBodyParameter("action", "release_activity");
		}
		params.addBodyParameter("id", (Integer) leibie.getTag() + "");
		params.addBodyParameter("trade_id",
				String.valueOf(userData.getTrade_id()));
		params.addBodyParameter("user_name", userData.getUser_name());
		params.addBodyParameter("password", userData.getPassword());
		params.addBodyParameter("type", fangshi.getTag() + "");
		try {
			params.addBodyParameter("activitytitle", str.get(0));
			params.addBodyParameter("tradename", str.get(1));
			params.addBodyParameter("quantity", str.get(4));
			if (Integer.valueOf(editHelper.getEditTexts().get(4).getText()
					.toString()) <= 0) {
				MyToast.show(this, "数量必须大于0", Toast.LENGTH_SHORT);
				return null;
			}
			params.addBodyParameter("address", str.get(5));
			params.addBodyParameter("endtime", str.get(6));
			params.addBodyParameter("mobile", str.get(7));
			params.addBodyParameter("activityintroduction", str.get(8));
			/*
			 * original_img=/upload/201511/08/thumb_201511081112015615.jpg,258|/
			 * upload/201511/09/201511091020209319.png|/upload/201511/09/
			 * thumb_201511091020209319.png,
			 * 261|/upload/201511/09/201511091021433216.
			 * png|/upload/201511/09/thumb_201511091021433216.png
			 */
			StringBuilder original_img = new StringBuilder();
			for (int i = 0; i < imgData.size() - 1; i++) {
				if (imgData.get(i).getSdPath() != null
						&& !imgData.get(i).getSdPath().equals("")) {
					File f = new File(imgData.get(i).getSdPath());
					if (f.exists()) {
						params.addBodyParameter("activityimg" + (i + 1), f);
						Log.e("File", i + "   " + f.getPath());
					}
				} else {

					original_img.append(imgData.get(i).getId());
					original_img.append("|");
					original_img.append(imgData.get(i).getOriginal_path());
					original_img.append("|");
					original_img.append(imgData.get(i).getThumb_path());
					original_img.append(",");
				}
			}
			if (original_img.length() > 1) {
				original_img.delete(original_img.length() - 1,
						original_img.length());
				params.addBodyParameter("original_img", original_img.toString());
			}
			MyLog.e("original_img", original_img.toString());
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}

		return params;
	}

	private void configIntent(Intent intent) {
		intent.putExtra("borderWidth", 2);
		intent.putExtra("clipPadding", 0);
		intent.putExtra("clipWidth", 2);
		intent.putExtra("clipHeight", 1);
		intent.putExtra("shared", ClipLayout.RECT);

	}

	private void getClaimType() {
		httpUtils.send(HttpMethod.GET, HttpInterfaces.activityType,
				new MyRequestCallBack<String>(this,
						getString(R.string.loadding)) {
					@Override
					public void onSuccess(ResponseInfo<String> responseInfo) {
						super.onSuccess(responseInfo);
						try {
							JSONObject json = new JSONObject(
									responseInfo.result);
							int status = json.getInt("status");
							if (status == 1) {
								Collection<ActivityTypeData> c = JsonHelper
										.parseCollection(
												json.getJSONArray("result"),
												ArrayList.class,
												ActivityTypeData.class);
								claimTypeList.addAll(c);
							}

						} catch (JSONException e) {
							e.printStackTrace();
						}

					}
				});
	}

	private void getActivityType() {
		httpUtils.send(HttpMethod.GET, HttpInterfaces.goodType,
				new MyRequestCallBack<String>(this,
						getString(R.string.loadding)) {
					@Override
					public void onSuccess(ResponseInfo<String> responseInfo) {
						super.onSuccess(responseInfo);
						try {
							JSONObject json = new JSONObject(
									responseInfo.result);
							int status = json.getInt("status");
							if (status == 1) {
								Collection<ActivityTypeData> c = JsonHelper
										.parseCollection(
												json.getJSONArray("result"),
												ArrayList.class,
												ActivityTypeData.class);
								activityTypeList.addAll(c);
							}

						} catch (JSONException e) {
							e.printStackTrace();
						}

					}
				});
	}

	private void getTimu() {
		httpUtils.send(HttpMethod.GET, HttpInterfaces.getTimu,
				new MyRequestCallBack<String>(this,
						getString(R.string.loadding)) {
					@Override
					public void onSuccess(ResponseInfo<String> responseInfo) {
						super.onSuccess(responseInfo);
						try {
							JSONObject json = new JSONObject(
									responseInfo.result);
							int status = json.getInt("status");
							if (status == 1) {
								Collection<TimuData> c = JsonHelper.parseCollection(
										json.getJSONArray("result"),
										ArrayList.class, TimuData.class);
								timuList.addAll(c);
							}

						} catch (JSONException e) {
							e.printStackTrace();
						}

					}
				});
	}

	public class MyLocationListener implements BDLocationListener {

		@Override
		public void onReceiveLocation(BDLocation location) {
			try {
				editHelper.getEditTexts().get(5).setText(location.getAddrStr());
			} catch (IndexOutOfBoundsException e) {
				e.printStackTrace();
			}
			mLocationClient.stop();

		}
	}

	/*
	 * 获取往期活动的详情
	 */
	private void getServiceActivity(int activity_id) {
		if (activity_id != -1) {
			httpUtils.send(HttpMethod.GET, HttpInterfaces.getActivity
					+ "&activity_id=" + activity_id,
					new MyRequestCallBack<String>(this,
							getString(R.string.loadding)) {
						@Override
						public void onSuccess(ResponseInfo<String> responseInfo) {
							super.onSuccess(responseInfo);
							try {
								JSONObject json = new JSONObject(
										responseInfo.result);
								int status = json.getInt("status");
								if (status == 1) {
									again = true;
									json = json.getJSONArray("result")
											.getJSONObject(0);
									ArrayList<TextView> eds = editHelper
											.getEditTexts();

									eds.get(0).setText(json.getString("title"));
									eds.get(1).setText(json.getString("name"));

									// 活动类型
									for (int i = 0; i < activityTypeList.size(); i++) {
										if (activityTypeList.get(i).getId() == json
												.getInt("activity_type")) {
											leibie.setText(activityTypeList
													.get(i).getTitle());
											leibie.setTag(activityTypeList.get(
													i).getId());
											break;
										}

									}
									// 活动方式
									for (int i = 0; i < claimTypeList.size(); i++) {
										if (claimTypeList.get(i).getType() == json
												.getInt("type")) {
											fangshi.setText(claimTypeList
													.get(i).getTitle());
											fangshi.setTag(claimTypeList.get(i)
													.getType());
											selectTypeUi(claimTypeList.get(i)
													.getType());
											break;
										}
									}
									eds.get(4)
											.setText(json.getString("number"));
									eds.get(5).setText(
											json.getString("address"));
									eds.get(6).setText(
											json.getString("end_time"));

									eds.get(7).setText(
											json.getString("telephone"));
									eds.get(8).setText(
											SystemTools.Html2Text(json
													.getString("content")));

									// 题目
									if (json.getInt("answer_id") != 0) {
										for (int i = 0; i < timuList.size(); i++) {
											if (timuList.get(i).getId() == json
													.getInt("answer_id")) {
												tiMu.setText(timuList.get(i)
														.getTitle());
												tiMu.setTag(timuList.get(i)
														.getId());
												break;
											}
										}
									}
									// 分享次数
									if (json.getInt("share_count") > 0) {
										sharedCount.setText(String.valueOf(json
												.getInt("share_count")));
									}

								}
							} catch (JSONException e) {
								e.printStackTrace();
							} catch (Exception e) {
								e.printStackTrace();
							}
						}
					});

			httpUtils.send(HttpMethod.GET, HttpInterfaces.goodPic
					+ "&activity_id=" + activity_id,
					new MyRequestCallBack<String>(this,
							getString(R.string.loadding)) {
						@Override
						public void onSuccess(ResponseInfo<String> responseInfo) {
							super.onSuccess(responseInfo);
							try {
								JSONObject json = new JSONObject(
										responseInfo.result);
								int status = json.getInt("status");
								if (status == 1) {
									Collection<ImagesData> datas = JsonHelper.parseCollection(
											json.getJSONArray("result"),
											ArrayList.class, ImagesData.class);
									imgData.addAll(0, datas);
									adapter.notifyDataSetChanged();
								}
							} catch (JSONException e) {
								e.printStackTrace();
							} catch (Exception e) {
								e.printStackTrace();
							}
						}
					});
		}
	}

	private int activity_id = -1;
	private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {

		@Override
		public void onReceive(Context context, Intent intent) {
			activity_id = intent.getIntExtra("activity_id", -1);
			getServiceActivity(activity_id);

		}

	};

	@Override
	protected void onResume() {
		super.onResume();
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		unregisterReceiver(broadcastReceiver);
	}
}
