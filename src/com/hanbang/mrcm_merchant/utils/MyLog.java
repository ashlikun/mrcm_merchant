package com.hanbang.mrcm_merchant.utils;

import android.util.Log;

public class MyLog {

	private static boolean debugLog = true;

	/*
	 * ����
	 */
	public static void e(String tag, String msg) {
		if (debugLog) {
			Log.e(tag, msg);
		}
	}

	/*
	 * ����
	 */
	public static void d(String tag, String msg) {
		if (debugLog) {
			Log.d(tag, msg);
		}
	}

	/*
	 * ȫ��
	 */
	public static void v(String tag, String msg) {
		if (debugLog) {
			Log.v(tag, msg);
		}
	}

	/*
	 * ����
	 */
	public static void w(String tag, String msg) {
		if (debugLog) {
			Log.w(tag, msg);
		}
	}

	/*
	 * ��Ϣ
	 */
	public static void i(String tag, String msg) {
		if (debugLog) {
			Log.i(tag, msg);
		}
	}
}
