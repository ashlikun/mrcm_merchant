package com.hanbang.mrcm_merchant.utils;

public class HttpInterfaces {

	public static String ORG = "http://mrcm.hbung.com";
	public static String GOOD_LIST_POST = "http://mrcm.hbung.com/tools/good_list.ashx?";
	/*
	 * 
	 * http://mrcm.hbung.com/tools/UserLogin.ashx?action=seller_login&user_name=
	 * 17051196428&password=123456 --商家登陆
	 */
	public static String userLogin = "http://mrcm.hbung.com/tools/UserLogin.ashx?";

	public static String updateApp = "";

	/*
	 * http://mrcm.hbung.com/tools/good_list.ashx?action=release_activity&id=3&
	 * user_name
	 * =yangyu&password=yangyu&type=2&activitytitle=锤子&tradename=大宇手机店&quantity
	 * =120
	 * &address=苏州市姑苏区&endtime=2015-10-30&mobile=17051196428&activityintroduction
	 * =好一个锤子手机&activityimg=123.jpg--发布活动
	 */

	/*
	 * 
	 * http://mrcm.hbung.com/tools/good_list.ashx?action=good_type --
	 * 发布时用的活动所属类别
	 */
	public static String goodType = "http://mrcm.hbung.com/tools/good_list.ashx?action=good_type";

	/*
	 * 
	 * 
	 * http://mrcm.hbung.com/tools/good_list.ashx?action=activity_type
	 * --发布时用的活动类别
	 */
	public static String activityType = "http://mrcm.hbung.com/tools/good_list.ashx?action=activity_type";

	/*
	 * http://mrcm.hbung.com/tools/good_list.ashx?action=past_activity&user_name=
	 * yangyu&password=yangyu--往期活动
	 */
	public static String pastActivity = "http://mrcm.hbung.com/tools/good_list.ashx?action=past_activity";
	/*
	 * http://mrcm.hbung.com/tools/good_list.ashx?action=delete_activity&user_name
	 * =yangyu&password=yangyu&past_activity_id=19--删除往期活动
	 */
	public static String deleteActivity = "http://mrcm.hbung.com/tools/good_list.ashx?action=delete_activity";

	/*
	 * http://mrcm.hbung.com/tools/good_list.ashx?action=validate_code&mobile=Mrcm
	 * &password=123456&code=ZX8Z40FZ
	 */
	public static String validateCode = "http://mrcm.hbung.com/tools/good_list.ashx?action=validate_code";
	/*
	 * http://mrcm.hbung.com/tools/good_list.ashx?action=specificactivity&
	 * activity_id=9 --具体某个活动
	 */
	public static String getActivity = "http://mrcm.hbung.com/tools/good_list.ashx?action=specificactivity";
	
	/*
	 * http://mrcm.hbung.com/tools/good_list.ashx?action=good_pic&activity_id=77--
	 * 具体某个活动 置顶
	 */
	public static String goodPic = "http://mrcm.hbung.com/tools/good_list.ashx?action=good_pic";

	/*
	 * http://mrcm.hbung.com/tools/good_list.ashx?action=getquestionbank --获取题库
	 */
	public static String getTimu = "http://mrcm.hbung.com/tools/good_list.ashx?action=getquestionbank";
	
	/*
	 *http://localhost:2642/tools/UserLogin.ashx?action=seller_register -- 商家注册 
	 */
	public static String seller_register = "http://mrcm.hbung.com/tools/UserLogin.ashx?action=seller_register";
	/*
	 *http://localhost:10080/tools/UserLogin.ashx?action=send_verify_sms_code -- 商家注册 验证码
	 */
	public static String send_verify_sms_code = "http://mrcm.hbung.com/tools/UserLogin.ashx?action=send_verify_sms_code";
	/*
	 *http://localhost:10080/tools/UserLogin.ashx?action=send_verify_sms_code --  修改商家密码
	 */
	public static String seller_update_pwd = "http://mrcm.hbung.com/tools/UserLogin.ashx?action=seller_update_pwd";
	/*
	 *http://localhost:10080/tools/UserLogin.ashx?action=send_verify_sms_code --  修改个人中心
	 */
	public static String seller_update = "http://mrcm.hbung.com/tools/UserLogin.ashx?action=seller_update";
}
