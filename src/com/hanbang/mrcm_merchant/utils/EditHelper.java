package com.hanbang.mrcm_merchant.utils;

import java.util.ArrayList;

import android.content.Context;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class EditHelper {

	private Context context;
	private ArrayList<TextView> editTexts = new ArrayList<TextView>();
	private ArrayList<String> hintTexts = new ArrayList<String>();
	private ArrayList<Integer> hintMins = new ArrayList<Integer>();

	public ArrayList<TextView> getEditTexts() {
		return editTexts;
	}

	public EditHelper(Context context) {
		this.context = context;
	}

	/*
	 * 设置监听的输入框
	 */
	public void setEditText(TextView... edits) {
		for (TextView e : edits) {
			editTexts.add(e);
		}

	}

	/*
	 * 设置提示文本
	 */
	public void setHintText(String... hints) {
		for (String e : hints) {
			hintTexts.add(e);
		}
	}

	/*
	 * 设置输入的最小个数
	 */
	public void setHintMinNumber(int... hints) {
		for (int e : hints) {
			hintMins.add(e);
		}
	}

	public ArrayList<String> getText() {
		ArrayList<String> s = new ArrayList<String>();
		for (TextView e : editTexts) {
			s.add(e.getText().toString().trim());
		}
		return s;

	}

	/*
	 * 检查是否满足
	 */
	public boolean check() {
		try {
			for (int i = 0; i < editTexts.size(); i++) {
				TextView e = editTexts.get(i);
				if (e.getText().toString().trim().length() < hintMins.get(i)) {
					MyToast.show(context, hintTexts.get(i), Toast.LENGTH_LONG);
					return false;
				}

			}
			return true;
		} catch (IndexOutOfBoundsException e) {
			e.printStackTrace();
		}

		return false;
	}

	/*
	 * 
	 */
	public void clear() {
		try {
			for (int i = 0; i < editTexts.size(); i++) {
				TextView e = editTexts.get(i);
				e.setText("");
				e.setTag(null);
			}
		} catch (IndexOutOfBoundsException e) {
			e.printStackTrace();
		}

	}
}
