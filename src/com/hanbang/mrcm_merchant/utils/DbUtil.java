package com.hanbang.mrcm_merchant.utils;

import android.content.Context;

import com.hanbang.mrcm_merchant.R;
import com.lidroid.xutils.DbUtils;
import com.lidroid.xutils.DbUtils.DaoConfig;

public class DbUtil {
	private static DbUtils dbUtils;

	public static DbUtils getDbUtils(Context context) {
		if (dbUtils == null) {
			DaoConfig daoConfig = new DaoConfig(context);
			// �������ݿ�����
			daoConfig.setDbName(context.getString(R.string.app_name));
			// �������ݿ�汾��
			daoConfig.setDbVersion(2);
			dbUtils = getDbUtils(context, daoConfig);
			return dbUtils;
		} else {
			return dbUtils;
		}
	}

	private static DbUtils getDbUtils(Context context, DaoConfig daoConfig) {
		DbUtils dbUtils = DbUtils.create(daoConfig);
		// ���õ��Դ�ӡ����
		dbUtils.configDebug(true);
		// �����Ƿ������������
		// �򵥾ٸ����Ӿ�����Ҫͬʱ�޸����ݿ���������ͬ���ʱ��������ǲ���һ������Ļ�������һ�����޸��꣬���ǵڶ�����޳������쳣��û���޸ĵ�����£���ֻ�еڶ�����ص�δ�޸�֮ǰ��״̬������һ�����Ѿ����޸���ϡ�
		dbUtils.configAllowTransaction(true);
		return dbUtils;
	}

}
