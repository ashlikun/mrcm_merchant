package com.hanbang.mrcm_merchant.utils;

import android.content.Context;
import android.view.Gravity;
import android.widget.Toast;

public class MyToast {

	private static Toast myToast = null;

	public static void show(Context context, String text, int duration) {
		initToast(context);
		if (myToast != null) {
			myToast.setGravity(Gravity.BOTTOM, SystemTools.dip2px(context, 0),
					SystemTools.dip2px(context, 60));
			myToast.setText(text);
			myToast.setDuration(duration);
			myToast.show();
		}

	}

	private static void initToast(Context context) {
		if (myToast == null) {
			myToast = Toast.makeText(context, "", Toast.LENGTH_SHORT);
		}
	}

	public static void show(Context context, String text, int duration,
			int gravity, int xOffsetDp, int yOffsetDp) {
		initToast(context);
		if (myToast != null) {
			myToast.setGravity(gravity, SystemTools.dip2px(context, xOffsetDp),
					SystemTools.dip2px(context, yOffsetDp));
			myToast.setText(text);
			myToast.setDuration(duration);
			myToast.show();
		}

	}
}
