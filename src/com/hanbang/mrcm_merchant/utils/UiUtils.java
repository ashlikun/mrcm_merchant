package com.hanbang.mrcm_merchant.utils;

import com.hanbang.mrcm_merchant.R;
import com.hanbang.mrcm_merchant.application.MyApplication;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnTouchListener;
import android.widget.ImageView;
import android.widget.TextView;

public class UiUtils {
	public static void setButton(ViewGroup ll, boolean press) {
		if (press) {
			ll.setEnabled(false);
			ll.getChildAt(0).setEnabled(false);
			setImageViewTint((ImageView) ll.getChildAt(0), R.color.red_shen);
			((TextView) (ll.getChildAt(1))).setTextColor(ll.getResources()
					.getColor(R.color.red_shen));
		} else {
			ll.setEnabled(true);
			ll.getChildAt(0).setEnabled(true);
			setImageViewTint((ImageView) ll.getChildAt(0), R.color.white);
			((TextView) (ll.getChildAt(1))).setTextColor(ll.getResources()
					.getColor(R.color.white));
		}
	}

	// /*
	// * 设置按钮点击后的渲染（Tint）
	// */
	// public static void setImageViewClickTint(ImageView view, int colorUp,
	// int colorDown) {
	// // StateListDrawable drawable = new StateListDrawable();
	// // Drawable down = view.getDrawable();
	// // down.setColorFilter(0xff0000, PorterDuff.Mode.SRC_ATOP);
	// // Drawable up = view.getDrawable();
	// // down.setColorFilter(colorUp, PorterDuff.Mode.SRC_ATOP);
	// // drawable.addState(new int[] {}, down);
	// // ((ImageView) view).setImageDrawable(drawable);
	// }

	@SuppressLint("ClickableViewAccessibility")
	public static void setButtonClickTint(final View view, final int colorUp,
			final int colorDown) {

		if (view instanceof ImageView) {
			view.setOnTouchListener(new OnTouchListener() {

				@Override
				public boolean onTouch(View v, MotionEvent event) {
					switch (event.getAction()) {
					case MotionEvent.ACTION_DOWN:
						((ImageView) view).setColorFilter(view.getResources()
								.getColor(colorDown));
						break;
					case MotionEvent.ACTION_CANCEL:
					case MotionEvent.ACTION_UP:
						((ImageView) view).setColorFilter(view.getResources()
								.getColor(colorUp));
						break;

					default:
						break;
					}

					return false;
				}
			});
			// 设置字体点击后的文字颜色变化
		} else if (view instanceof TextView) {
			view.setOnTouchListener(new OnTouchListener() {

				@SuppressLint("ClickableViewAccessibility")
				@Override
				public boolean onTouch(View v, MotionEvent event) {
					switch (event.getAction()) {

					case MotionEvent.ACTION_DOWN:
						((TextView) view).setTextColor(view.getResources()
								.getColor(colorDown));
						break;
					case MotionEvent.ACTION_CANCEL:
					case MotionEvent.ACTION_UP:
						((TextView) view).setTextColor(view.getResources()
								.getColor(colorUp));
						break;

					default:
						break;
					}

					return false;
				}
			});
		}
	}

	/*
	 * 设置ImageView渲染（Tint）
	 */
	public static void setImageViewTint(final ImageView view, final int color) {
		view.setColorFilter(view.getResources().getColor(color));
	}

	/*
	 * 设置字体
	 */
	public static void applyFont(final View root) {
		try {
			if (root instanceof ViewGroup) {
				ViewGroup viewGroup = (ViewGroup) root;
				for (int i = 0; i < viewGroup.getChildCount(); i++)
					applyFont(viewGroup.getChildAt(i));
			} else if (root instanceof TextView)
				((TextView) root).setTypeface(((MyApplication) (root
						.getContext().getApplicationContext())).typeface);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static View getRootView(Activity context) {
		return context.getWindow().getDecorView()
				.findViewById(android.R.id.content);
	}

}