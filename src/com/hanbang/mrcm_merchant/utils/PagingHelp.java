package com.hanbang.mrcm_merchant.utils;

import java.util.List;
import com.hanbang.mrcm_merchant.R;
import android.content.Context;
import android.widget.Toast;

public class PagingHelp {

	// 总条数
	private int recordCount = 0;
	// 服务器数据的第几页
	private int pageindex = 1;
	// 上一次加载的第几条
	private int currentCount = 0;
	// 每页多少条
	private int pageCount = 10;

	public PagingHelp() {
	}

	public boolean isNextPaging(Context context, int result) {
		if (currentCount == result) {
			// 没有跟多数据可以加载
			if (result != 0) {
				MyToast.show(context, context.getString(R.string.no_more_data),
						Toast.LENGTH_LONG);
				return false;
			}
		} else {
			currentCount = result;
			if (currentCount == pageCount) {
				// 如果加载满一页
				currentCount = 0;
				pageindex++;
			}
		}
		return true;
	}

	public List<Object> getValidData(Context context, List<Object> c) {
		isNextPaging(context, c.size());
		int max = (currentCount <= c.size()) ? currentCount : c.size();
		for (int i = 0; i < max; i++) {
			c.remove(i);
		}
		return c;
	}

	public void showNoData(Context context) {
		MyToast.show(context, context.getString(R.string.no_more_data),
				Toast.LENGTH_LONG);
	}

	/*
	 * 把数据清空 恢复到开始时的状态
	 */
	public void clear() {
		// 总条数
		recordCount = 0;
		// 服务器数据的第几页
		pageindex = 1;
		// 上一次加载的第几条
		currentCount = 0;
		// 每页多少条
		pageCount = 10;
	}

	public int getRecordCount() {
		return recordCount;
	}

	public void setRecordCount(int recordCount) {
		this.recordCount = recordCount;
	}

	public int getPageindex() {
		return pageindex;
	}

	public void setPageindex(int pageindex) {
		this.pageindex = pageindex;
	}

	public int getCurrentCount() {
		return currentCount;
	}

	public void setCurrentCount(int currentCount) {
		this.currentCount = currentCount;
	}

	public int getPageCount() {
		return pageCount;
	}

	public void setPageCount(int pageCount) {
		this.pageCount = pageCount;
	}

}
