package com.hanbang.mrcm_merchant.utils;

import android.content.Context;
import android.widget.Toast;

import com.hanbang.mrcm_merchant.R;
import com.hanbang.mrcm_merchant.myview.MyDialog;
import com.hanbang.mrcm_merchant.myview.MyDialog.MODE;
import com.lidroid.xutils.exception.HttpException;
import com.lidroid.xutils.http.ResponseInfo;
import com.lidroid.xutils.http.callback.RequestCallBack;

public abstract class MyRequestCallBack<T> extends RequestCallBack<T> {
	private Context context = null;
	private String dialogContent = "";
	private MyDialog myDialog = null;

	public final void setRateX(int rate) {
		super.setRate(rate);
	}

	@Override
	public void onCancelled() {
		super.onCancelled();
	}

	/*
	 * 构造函数 参数是准备创建对话框 如果content为null 那么就代表这条http请求不弹出对话框
	 */
	public MyRequestCallBack(Context context, String content) {

		this.context = context;
		dialogContent = content;
	}

	@Override
	public void onStart() {
		super.onStart();
		// 判断是否加载对话框
		if (context != null & dialogContent != null) {
			myDialog = new MyDialog(context, R.style.base_dialog, MODE.LOADDING);
			myDialog.setContent(dialogContent);
			myDialog.show();
		}
	}
	@Override
	public void onSuccess(ResponseInfo<T> responseInfo) {
		MyLog.e("onSuccess", responseInfo.result.toString());
		if (myDialog != null) {
			myDialog.dismiss();
		}
	}

	@Override
	public void onFailure(HttpException error, String msg) {
		MyLog.e("onFailure", msg + " code" + error.getExceptionCode());
		if (myDialog != null) {
			myDialog.dismiss();
		}
		if (error.getExceptionCode() == 0) {
			MyToast.show(context, "服务器无响应", Toast.LENGTH_SHORT);
		}
	}

	@Override
	public void onLoading(long total, long current, boolean isUploading) {
		super.onLoading(total, current, isUploading);
		MyLog.e("onLoading", "总长：" + total + "   加载到：" + current);
	}
}
