package com.hanbang.mrcm_merchant.convert;

import org.json.JSONException;
import org.json.JSONObject;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import com.hanbang.mrcm_merchant.R;
import com.hanbang.mrcm_merchant.announce.AnnounceActivity;
import com.hanbang.mrcm_merchant.base.MainBaseActivity;
import com.hanbang.mrcm_merchant.myview.MyDialog;
import com.hanbang.mrcm_merchant.old_activity.OldActivityMeActivity;
import com.hanbang.mrcm_merchant.utils.HttpInterfaces;
import com.hanbang.mrcm_merchant.utils.MyRequestCallBack;
import com.hanbang.mrcm_merchant.utils.MyToast;
import com.lidroid.xutils.http.ResponseInfo;
import com.lidroid.xutils.http.client.HttpRequest.HttpMethod;

public class ConvertActivity extends MainBaseActivity {
	private EditText codeET = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.convert);
		initView();
		setOnClickListener();
	}

	/*
	 * 设置点击事件
	 */
	private void setOnClickListener() {
		ViewGroup v1 = (ViewGroup) findViewById(R.id.bottom_bar_ll1);
		ViewGroup v2 = (ViewGroup) findViewById(R.id.bottom_bar_ll2);
		ViewGroup v3 = (ViewGroup) findViewById(R.id.bottom_bar_ll3);
		setBottonButton(v1);
		v2.setOnClickListener(onClickListener);
		v3.setOnClickListener(onClickListener);
		findViewById(R.id.ok).setOnClickListener(onClickListener);
	}

	private OnClickListener onClickListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			Intent intent = new Intent();
			switch (v.getId()) {
			case R.id.bottom_bar_ll2:
				intent.setClass(ConvertActivity.this, AnnounceActivity.class);
				intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
				startActivity(intent);
				break;
			case R.id.bottom_bar_ll3:
				intent.setClass(ConvertActivity.this,
						OldActivityMeActivity.class);
				intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
				startActivity(intent);
				break;
			case R.id.ok:
				if (!codeET.getText().toString().trim().equals("")) {
					getClaimType(codeET.getText().toString().trim());
				} else {
					MyToast.show(ConvertActivity.this, "请输入兑换码",
							Toast.LENGTH_SHORT);
				}
				break;
			default:
				break;
			}
		}
	};

	private void getClaimType(String code) {
		if (!isLogin(true)) {
			return;
		}
		httpUtils.send(HttpMethod.POST, HttpInterfaces.validateCode
				+ "&mobile=" + userData.getUser_name() + "&password="
				+ userData.getPassword() + "&code=" + code,
				new MyRequestCallBack<String>(this,
						getString(R.string.loadding)) {
					@Override
					public void onSuccess(ResponseInfo<String> responseInfo) {
						super.onSuccess(responseInfo);
						try {
							JSONObject json = new JSONObject(
									responseInfo.result);
							int status = json.getInt("status");
							MyDialog dialog = new MyDialog(
									ConvertActivity.this, R.style.base_dialog,
									MyDialog.MODE.CONFIRM);
							dialog.setTitle("兑换结果");
							dialog.setOnClickCallback(new MyDialog.OnClickCallback() {

								@Override
								public void onClick(MyDialog dialog,
										Object number, Object content) {
									codeET.setText("");
								}
							});
							if (status == 1 || status == 2 || status == 0) {
								dialog.setContent(json.getString("msg"));
								dialog.show();
							} else if (status == -100) {
								passwordIsError(false);
							}

						} catch (JSONException e) {
							e.printStackTrace();
						}

					}
				});
	}

	/*
	 * 初始化view
	 */
	private void initView() {
		// 设置顶部的标题
		setTitle(findViewById(R.id.top_bar_title),
				getString(R.string.botton_tab1));
		codeET = (EditText) findViewById(R.id.code);

	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
	}
}
