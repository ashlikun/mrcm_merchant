package com.hanbang.mrcm_merchant.base;

import com.hanbang.mrcm_merchant.application.ActivityManager;
import com.hanbang.mrcm_merchant.application.MyApplication;
import com.hanbang.mrcm_merchant.bean.UserInforData;
import com.hanbang.mrcm_merchant.else_module.LogInActivity;
import com.hanbang.mrcm_merchant.utils.BitmapUtil;
import com.hanbang.mrcm_merchant.utils.DbUtil;
import com.hanbang.mrcm_merchant.utils.MyLog;
import com.hanbang.mrcm_merchant.utils.MySharedPreferences;
import com.hanbang.mrcm_merchant.utils.MyToast;
import com.hanbang.mrcm_merchant.utils.UiUtils;
import com.lidroid.xutils.BitmapUtils;
import com.lidroid.xutils.DbUtils;
import com.lidroid.xutils.HttpUtils;
import com.lidroid.xutils.exception.DbException;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;
import android.widget.Toast;

public class BaseActivity extends Activity {
	private String TAG = BaseActivity.class.getSimpleName();
	public BitmapUtils bitmapUtils = null;
	public HttpUtils httpUtils = null;
	public DbUtils dbUtils = null;
	public UserInforData userData = null;
	private boolean isLoadFont = true;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		bitmapUtils = BitmapUtil.getBitmapUtils(BaseActivity.this,
				MyApplication.bitmapCachePath);
		httpUtils = ((MyApplication) getApplicationContext()).httpUtils;
		httpUtils.configSoTimeout(1000 * 15);
		dbUtils = DbUtil.getDbUtils(this);
		getDbUserData();
		ActivityManager.getInstance().pushActivity(this);
	}

	/*
	 * 设置回退键
	 */
	public void setBackArrows(View back) {
		back.setVisibility(View.VISIBLE);
		back.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				finish();
			}
		});
	}

	public void getDbUserData() {
		int id = MySharedPreferences.getSharedPreferencesKeyAndValue(this,
				MySharedPreferences.USER_DATA, "user_id", -1);
		if (id != -1) {
			try {
				userData = DbUtil.getDbUtils(this).findById(
						UserInforData.class, id);
			} catch (DbException e) {
				e.printStackTrace();
				userData = null;
			}
		} else {
			userData = null;
		}
	}

	public void setDbUserData() {
		if (userData != null) {
			try {
				DbUtil.getDbUtils(this).saveOrUpdate(userData);
			} catch (DbException e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		getDbUserData();
	};

	public boolean isLogin(boolean isToLogin) {
		if (userData == null) {
			if (isToLogin) {
				Intent intent = new Intent(this, LogInActivity.class);
				/*
				 * 返回的是124
				 */
				startActivityForResult(intent, 123);
			}
			MyLog.e(TAG, "未登录");
			return false;
		} else {
			MyLog.e(TAG, "已登录");
			return true;
		}
	}

	public void passwordIsError(boolean isToast) {
		MySharedPreferences.setSharedPreferencesKeyAndValue(BaseActivity.this,
				MySharedPreferences.USER_DATA, "user_id", -1);
		userData = null;
		if (isToast) {
			MyToast.show(this, "用户名或密码错误,请重新登录!", Toast.LENGTH_SHORT);
		}
	}

	/*
	 * 设置回退键
	 */
	public void setTitle(View title, String t) {

		title.setVisibility(View.VISIBLE);
		((TextView) title).setText(t);
	}

	@Override
	protected void onResume() {
		super.onResume();
		getDbUserData();
	}

	@Override
	protected void onStart() {
		super.onStart();
		if (isLoadFont) {
			UiUtils.applyFont(UiUtils.getRootView(this));
			isLoadFont = false;
		}
	}

	@Override
	protected void onPause() {
		super.onPause();
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		ActivityManager.getInstance().exitActivity(this);
	}

	@Override
	public void onLowMemory() {
		super.onLowMemory();
		ActivityManager.getInstance().exitActivity(this);
	}

}
