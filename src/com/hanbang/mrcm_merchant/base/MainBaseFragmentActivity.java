package com.hanbang.mrcm_merchant.base;

import com.hanbang.mrcm_merchant.application.ActivityManager;
import com.hanbang.mrcm_merchant.utils.MyToast;
import com.hanbang.mrcm_merchant.utils.UiUtils;

import android.content.Context;
import android.os.Bundle;
import android.view.ViewGroup;
import android.widget.Toast;

public class MainBaseFragmentActivity extends BaseFragmentActivity {
	private long exitTime = 0;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

	}

	public void setBottonButton(ViewGroup current) {
		UiUtils.setButton(current, true);
	}

	@Override
	protected void onResume() {
		super.onResume();
	}

	@Override
	protected void onPause() {
		super.onPause();
	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
	}

	@Override
	public void onBackPressed() {
		isExitApplication(this);
	}

	public void isExitApplication(Context context) {
		if ((System.currentTimeMillis() - exitTime) > 2000) {
			MyToast.show(this, "再按一次退出程序", Toast.LENGTH_SHORT);
			exitTime = System.currentTimeMillis();
		} else {
			// 退出
			ActivityManager.getInstance().exitAllActivity();
		}
	}
}
