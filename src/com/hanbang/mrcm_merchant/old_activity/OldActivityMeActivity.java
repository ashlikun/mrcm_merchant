package com.hanbang.mrcm_merchant.old_activity;

import java.util.ArrayList;
import java.util.Collection;
import org.json.JSONException;
import org.json.JSONObject;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import com.hanbang.mrcm_merchant.R;
import com.hanbang.mrcm_merchant.announce.AnnounceActivity;
import com.hanbang.mrcm_merchant.announce.PersonalActivity;
import com.hanbang.mrcm_merchant.base.MainBaseActivity;
import com.hanbang.mrcm_merchant.bean.OldActivityListItemData;
import com.hanbang.mrcm_merchant.convert.ConvertActivity;
import com.hanbang.mrcm_merchant.myview.MyDialog;
import com.hanbang.mrcm_merchant.myview.ScaleImageView;
import com.hanbang.mrcm_merchant.utils.HttpInterfaces;
import com.hanbang.mrcm_merchant.utils.JsonHelper;
import com.hanbang.mrcm_merchant.utils.MyRequestCallBack;
import com.hanbang.mrcm_merchant.utils.MyToast;
import com.hanbang.mrcm_merchant.utils.SystemTools;
import com.hanbang.mrcm_merchant.utils.UiUtils;
import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshBase.Mode;
import com.handmark.pulltorefresh.library.PullToRefreshBase.OnRefreshListener2;
import com.handmark.pulltorefresh.library.PullToRefreshListView;
import com.lidroid.xutils.exception.HttpException;
import com.lidroid.xutils.http.ResponseInfo;
import com.lidroid.xutils.http.client.HttpRequest.HttpMethod;

public class OldActivityMeActivity extends MainBaseActivity {

	public static String AGAIN_BROADCAST = "AGAIN_BROADCAST";
	private PullToRefreshListView listView = null;
	private ArrayList<OldActivityListItemData> listData = new ArrayList<OldActivityListItemData>();
	private MyAdapter adapter = null;
	private TextView lefttext;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.old_activity);
		initView();
		setOnClickListener();
		getListData(true);
	}

	/*
	 * 设置点击事件
	 */
	private void setOnClickListener() {
		ViewGroup v1 = (ViewGroup) findViewById(R.id.bottom_bar_ll1);
		ViewGroup v2 = (ViewGroup) findViewById(R.id.bottom_bar_ll2);
		ViewGroup v3 = (ViewGroup) findViewById(R.id.bottom_bar_ll3);
		setBottonButton(v3);
		v1.setOnClickListener(onClickListener);
		v2.setOnClickListener(onClickListener);
		lefttext.setOnClickListener(onClickListener);
		listView.setOnRefreshListener(new OnRefreshListener2<ListView>() {

			@Override
			public void onPullDownToRefresh(
					PullToRefreshBase<ListView> refreshView) {
				listData.clear();
				getListData(true);

			}

			@Override
			public void onPullUpToRefresh(
					PullToRefreshBase<ListView> refreshView) {

			}
		});
	}

	private OnClickListener onClickListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			Intent intent = new Intent();
			switch (v.getId()) {
			case R.id.bottom_bar_ll1:
				intent.setClass(OldActivityMeActivity.this,
						ConvertActivity.class);
				intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
				startActivity(intent);
				break;
			case R.id.bottom_bar_ll2:
				intent.setClass(OldActivityMeActivity.this,
						AnnounceActivity.class);
				intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
				startActivity(intent);
				break;

			case R.id.left_text:
				if(isLogin(false)){
					intent.setClass(OldActivityMeActivity.this, PersonalActivity.class);
					startActivity(intent);
				}else{
					MyToast.show(OldActivityMeActivity.this, "您未登入", Toast.LENGTH_SHORT);
				}
				 
				break;
			case R.id.right_text:
				passwordIsError(false);
				isLogin(true);
				break;
			default:
				break;
			}
		}
	};

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if (requestCode == 123 && resultCode == 124) {
			listData.clear();
			getListData(false);
		}
	};

	/*
	 * 初始化view
	 */
	private void initView() {
		// 设置顶部的标题
		setTitle(findViewById(R.id.top_bar_title),
				getString(R.string.botton_tab3));
		lefttext=(TextView) findViewById(R.id.left_text);
		lefttext.setVisibility(View.VISIBLE);
		lefttext.setText("个人中心");
		// 切换账号

		TextView tt = (TextView) findViewById(R.id.right_text);
		tt.setVisibility(View.VISIBLE);
		tt.setText("切换账号");
		UiUtils.setButtonClickTint(tt, R.color.white, R.color.gray);
		tt.setOnClickListener(onClickListener);

		listView = (PullToRefreshListView) findViewById(R.id.listview);
		listView.setMode(Mode.PULL_FROM_START);

		listView.getRefreshableView().setDivider(
				getResources().getDrawable(R.color.translucent));
		listView.getRefreshableView().setDividerHeight(
				SystemTools.dip2px(this, 10));
		adapter = new MyAdapter();
		listView.setAdapter(adapter);

	}

	private class MyAdapter extends BaseAdapter {

		@Override
		public int getCount() {
			return listData.size();
		}

		@Override
		public Object getItem(int position) {
			// TODO Auto-generated method stub
			return listData.get(position);
		}

		@Override
		public long getItemId(int position) {
			return 0;
		}

		@Override
		public View getView(final int position, View convertView,
				ViewGroup parent) {
			ViewHolder holder = null;
			if (convertView == null) {
				convertView = getLayoutInflater().inflate(
						R.layout.old_activity_list_item, null);
				holder = new ViewHolder();

				holder.imageView = (ScaleImageView) convertView
						.findViewById(R.id.image);
				holder.again = (TextView) convertView.findViewById(R.id.again);
				holder.delete = (TextView) convertView
						.findViewById(R.id.delete);

				convertView.setTag(holder);
			} else {
				holder = (ViewHolder) convertView.getTag();
			}
			bitmapUtils.display(holder.imageView, SystemTools
					.getHttpFileUrl(listData.get(position).getImg_url()));

			holder.delete.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					MyDialog dialog = new MyDialog(OldActivityMeActivity.this,
							R.style.base_dialog, MyDialog.MODE.CONFIRM);
					dialog.setTitle("确认删除");
					dialog.setContent("确定删除当前活动吗？");
					dialog.setOnClickCallback(new MyDialog.OnClickCallback() {

						@Override
						public void onClick(MyDialog dialog, Object number,
								Object content) {
							if ((Integer) number == 2) {
								try {
									deleteActivity(listData.get(position));
								} catch (IndexOutOfBoundsException e) {
								}

							}
						}
					});
					dialog.show();

				}
			});
			holder.again.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					Intent intent = new Intent();
					intent.setClass(OldActivityMeActivity.this,
							AnnounceActivity.class);
					intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);

					startActivity(intent);
					Intent intent2 = new Intent();
					intent2.setAction(AGAIN_BROADCAST);
					intent2.putExtra("activity_id", listData.get(position)
							.getPast_activity_id());
					sendBroadcast(intent2);
				}
			});

			return convertView;
		}
	}

	private class ViewHolder {
		ScaleImageView imageView = null;
		TextView delete = null;
		TextView again = null;
	}

	private void deleteActivity(final OldActivityListItemData data) {
		if (!isLogin(true)) {
			return;
		}

		httpUtils.send(
				HttpMethod.GET,
				HttpInterfaces.deleteActivity + "&trade_id="
						+ userData.getTrade_id() + "&user_name="
						+ userData.getUser_name() + "&password="
						+ userData.getPassword() + "&past_activity_id="
						+ data.getPast_activity_id(),
				new MyRequestCallBack<String>(this,
						getString(R.string.loadding)) {
					@Override
					public void onSuccess(ResponseInfo<String> responseInfo) {
						super.onSuccess(responseInfo);
						try {
							JSONObject json = new JSONObject(
									responseInfo.result);
							int status = json.getInt("status");
							if (status == 1) {
								MyToast.show(OldActivityMeActivity.this,
										json.getString("msg"),
										Toast.LENGTH_SHORT);
								listData.remove(data);
								adapter.notifyDataSetChanged();
							} else if (status == 0) {
								MyToast.show(OldActivityMeActivity.this,
										json.getString("msg"),
										Toast.LENGTH_SHORT);
							} else if (status == -100) {
								passwordIsError(false);
							}

						} catch (JSONException e) {
							e.printStackTrace();
						}

					}
				});
	}

	private void getListData(boolean isLogin) {
		if (!isLogin(isLogin)) {
			listView.onRefreshComplete();
			return;
		}

		httpUtils.send(HttpMethod.POST, HttpInterfaces.pastActivity
				+ "&user_name=" + userData.getUser_name() + "&password="
				+ userData.getPassword(), new MyRequestCallBack<String>(this,
				getString(R.string.loadding)) {

			@Override
			public void onFailure(HttpException error, String msg) {
				super.onFailure(error, msg);
				listView.onRefreshComplete();
			}

			@Override
			public void onSuccess(ResponseInfo<String> responseInfo) {
				super.onSuccess(responseInfo);
				try {
					JSONObject json = new JSONObject(responseInfo.result);
					int status = json.getInt("status");
					if (status == 1) {
						Collection<OldActivityListItemData> c = JsonHelper
								.parseCollection(json.getJSONArray("result"),
										ArrayList.class,
										OldActivityListItemData.class);
						listData.addAll(c);
						adapter.notifyDataSetChanged();
					}

				} catch (JSONException e) {
					e.printStackTrace();
				}
				listView.onRefreshComplete();
			}
		});

	}

	@Override
	protected void onResume() {
		super.onResume();
		TextView tt = (TextView) findViewById(R.id.right_text);
		tt.setVisibility(View.VISIBLE);
		if (isLogin(false)) {
			tt.setText("切换账号");
		} else {
			tt.setText("登录");
		}
	}

	@Override
	protected void onPause() {
		super.onPause();
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
	}
}
