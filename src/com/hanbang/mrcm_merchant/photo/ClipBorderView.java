package com.hanbang.mrcm_merchant.photo;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.View;

/**
 * 裁剪的边框
 */
public class ClipBorderView extends View {
	private float clipPadding;
	/*
	 * 边框的颜色
	 */
	private int borderColor;
	/*
	 * 边框形状
	 */
	private int borderShape;
	/*
	 * 边框宽度
	 */
	private float borderWidth = 1;
	private float width = 1;
	private float height = 1;
	// 高度是宽度的几倍
	private double heightIsWidth = 1;

	private float xCircl = 0;
	private float yCircl = 0;

	public ClipBorderView(Context context) {
		this(context, null);
	}

	public ClipBorderView(Context context, AttributeSet attrs) {
		this(context, attrs, 0);
	}

	public ClipBorderView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
	}

	@Override
	protected void onSizeChanged(int w, int h, int oldw, int oldh) {
		super.onSizeChanged(w, h, oldw, oldh);
		if (getRight() - getLeft() > 0 && getBottom() - getTop() > 0) {
			xCircl = (getRight() - getLeft()) / 2;
			yCircl = (getBottom() - getTop()) / 2;
			width = xCircl - clipPadding - borderWidth;
			if (borderShape == ClipLayout.ROUND) {
				height = width;
			} else if (borderShape == ClipLayout.RECT) {
				height = Math.round(width * heightIsWidth);
			}
			invalidate();
		}
	}

	@Override
	protected void onDraw(Canvas canvas) {
		super.onDraw(canvas);
		switch (borderShape) {
		case ClipLayout.ROUND:
			drawRound(canvas);
			break;
		case ClipLayout.RECT:
			drawRect(canvas);
			break;

		default:
			break;
		}

	}

	private void drawRound(Canvas canvas) {
		// 创建画笔
		Paint mPaint = new Paint();
		mPaint.setAntiAlias(true);
		// 设置画笔style为画实心的
		mPaint.setStyle(Paint.Style.STROKE);
		mPaint.setStrokeWidth(borderWidth);
		mPaint.setColor(borderColor);
		// mPaint.setARGB(255, 255, 255, 255);
		// 画圆
		canvas.drawCircle(xCircl, yCircl, width, mPaint);
	}

	private void drawRect(Canvas canvas) {
		Paint paint = new Paint();
		paint.setAntiAlias(true);
		paint.setStyle(Paint.Style.STROKE);
		paint.setStrokeWidth(borderWidth);
		paint.setColor(borderColor);
		canvas.drawRect(xCircl - width, yCircl - height, xCircl + width, yCircl
				+ height, paint);
	}

	public void setHorizontalPadding(int clipPadding) {
		this.clipPadding = clipPadding;
	}

	public void setBorderColor(int borderColor) {
		this.borderColor = borderColor;
	}

	public void setBorderShape(int borderShape) {
		this.borderShape = borderShape;
	}

	public void setBorderWidth(float borderWidth) {
		this.borderWidth = borderWidth;
	}

	public double getHeightIsWidth() {
		return heightIsWidth;
	}

	public void setHeightIsWidth(double heightIsWidth) {
		this.heightIsWidth = heightIsWidth;
	}

}
