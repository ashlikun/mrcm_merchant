package com.hanbang.mrcm_merchant.photo;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import com.hanbang.mrcm_merchant.R;
import com.hanbang.mrcm_merchant.application.MyApplication;
import com.hanbang.mrcm_merchant.base.BaseActivity;
import com.hanbang.mrcm_merchant.utils.BitmapUtil;
import com.hanbang.mrcm_merchant.utils.MyToast;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Toast;

public class ClipPhotoActivity extends BaseActivity {
	private ClipLayout clipLayout = null;
	// 照片存储路径
	private String filepath = MyApplication.picFilepath;
	private String clipPath = "";
	private int borderWidth = 2;
	private int clipPadding = 50;
	private int clipWidth = 1;
	private int clipHeight = 1;
	private int shared = ClipLayout.ROUND;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.clip_photo);
		clipPath = System.currentTimeMillis() + ".jpg";
		borderWidth = getIntent().getIntExtra("borderWidth", borderWidth);
		clipPadding = getIntent().getIntExtra("clipPadding", clipPadding);
		clipWidth = getIntent().getIntExtra("clipWidth", clipWidth);
		clipHeight = getIntent().getIntExtra("clipHeight", clipHeight);
		shared = getIntent().getIntExtra("shared", shared);

		initView();
		getPath();
	}

	private void getPath() {
		new Handler().postDelayed(new Runnable() {
			public void run() {
				try {
					String path = getIntent().getStringExtra("path");
					if (path != null) {
						Bitmap bitmap = BitmapUtil.decodeFile(new File(path),
								getWindowManager().getDefaultDisplay()
										.getWidth(), getWindowManager()
										.getDefaultDisplay().getHeight());
						clipLayout.setClipPhoto(bitmap);
						clipLayout.show();
					} else {

					}
				} catch (Exception e) {
					e.printStackTrace();
					MyToast.show(ClipPhotoActivity.this, "获取照片失败！",
							Toast.LENGTH_SHORT);
					finish();
				}

			}
		}, 30);// 30毫秒用于等待解码图片，要对唱时间可以自己设置

	}

	private void initView() {
		setBackArrows(findViewById(R.id.top_bar_left_image));
		findViewById(R.id.ok).setOnClickListener(myOncClickListener);

		clipLayout = (ClipLayout) findViewById(R.id.clipLayout);
		clipLayout.setBorderColor(0xffff0000);
		clipLayout.setBorderShape(shared);
		clipLayout.setClipPadding(clipPadding);
		clipLayout.setBorderWidth(borderWidth);
		clipLayout.setWidthToHeightRatio(clipWidth, clipHeight);
	}

	private OnClickListener myOncClickListener = new OnClickListener() {
		@Override
		public void onClick(View view) {
			switch (view.getId()) {
			case R.id.ok:
				saveBitmap();
				break;
			default:
				break;
			}
		}
	};

	/*
	 * 保存图片 先上传到服务器 成功了就保存到本地
	 */
	private void saveBitmap() {
		Bitmap bitmap = clipLayout.clip();
		if (bitmap != null) {
			String path = saveToSd(bitmap, clipPath);
			if (bitmap.isRecycled()) {
				bitmap.recycle();
				bitmap = null;
			}
			if (path != null && !path.equals("")) {
				Intent intent = new Intent();
				intent.putExtra("path", path);
				setResult(1000, intent);
				finish();
			}
		} else {
			// 失败
			setResult(1001);
			finish();
		}
	}

	@Override
	protected void onResume() {
		super.onResume();
	}

	@Override
	protected void onPause() {
		super.onPause();
	}

	@Override
	protected void onStop() {
		super.onStop();
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
	}

	/*
	 * 保存到本地，当保存到服务器成功的时候就保存到本地 不成功就不保存
	 */
	private String saveToSd(Bitmap bitmap, String name) {
		// 先创建文件路径，应为创建文件的时候前提是文件路径存在
		File path = new File(filepath);
		File file = null;
		if (path.exists() || path.mkdirs()) {
			Log.e("mkdirs", path.getPath());
			// 创建保存的图片名称
			file = new File(path, name);
			if (!file.exists()) {
				try {
					file.createNewFile();
				} catch (IOException e) {
					e.printStackTrace();
					return "";
				}
			}
		}
		FileOutputStream fos = null;
		try {
			fos = new FileOutputStream(file);
			bitmap.compress(Bitmap.CompressFormat.JPEG, 100, fos);
		} catch (Exception e) {
			e.printStackTrace();
			return "";
		} finally {
			if (fos != null) {
				try {
					fos.flush();
					fos.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
				fos = null;
			}
		}
		return file.getPath();
	}

}