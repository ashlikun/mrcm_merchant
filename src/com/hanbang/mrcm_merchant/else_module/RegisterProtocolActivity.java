package com.hanbang.mrcm_merchant.else_module;

import org.json.JSONException;
import org.json.JSONObject;
import android.annotation.SuppressLint;
import android.os.Bundle;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebSettings.LayoutAlgorithm;
import com.hanbang.mrcm_merchant.R;
import com.hanbang.mrcm_merchant.base.BaseActivity;
import com.hanbang.mrcm_merchant.utils.HttpInterfaces;
import com.hanbang.mrcm_merchant.utils.MyLog;
import com.hanbang.mrcm_merchant.utils.MyRequestCallBack;
import com.lidroid.xutils.http.ResponseInfo;
import com.lidroid.xutils.http.client.HttpRequest.HttpMethod;

public class RegisterProtocolActivity extends BaseActivity {

	private WebView webview = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.register_protocol);
		initView();
		setOnClickListener();
		getProtocol();

	}

	/*
	 * 设置点击事件
	 */
	private void setOnClickListener() {

	}

	/*
	 * 初始化view
	 */
	private void initView() {
		// 设置顶部的标题
		setBackArrows(findViewById(R.id.top_bar_left_image));
		setTitle(findViewById(R.id.top_bar_title), "注册协议");

		webview = (WebView) findViewById(R.id.webview);
		configWebview();

	}

	/*
	 * http://localhost:10080/tools/UserLogin.ashx?action=registration_agreement
	 * --注册协议
	 */
	private void getProtocol() {
		httpUtils
				.send(HttpMethod.GET,
						"http://mrcm.hbung.com/tools/UserLogin.ashx?action=registration_agreement",
						new MyRequestCallBack<String>(this,
								getString(R.string.loadding)) {
							@Override
							public void onSuccess(
									ResponseInfo<String> responseInfo) {
								super.onSuccess(responseInfo);
								try {
									JSONObject json = new JSONObject(
											responseInfo.result);
									int status = json.getInt("status");
									if (status == 1) {
										String str = json
												.getJSONArray("result")
												.getJSONObject(0)
												.getString("content");
										upDateUI(str);
									}
								} catch (JSONException e) {
									e.printStackTrace();
								} catch (IndexOutOfBoundsException e) {
									e.printStackTrace();
								}
							}
						});
	}

	@SuppressLint("SetJavaScriptEnabled")
	private void configWebview() {
		// 设置文本编码
		webview.getSettings().setDefaultTextEncodingName("utf-8");
		// 可以缩放
		webview.getSettings().setSupportZoom(true);
		webview.getSettings().setJavaScriptEnabled(true);
		webview.getSettings().setLayoutAlgorithm(LayoutAlgorithm.SINGLE_COLUMN);
		webview.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);
		// 开启 Application Caches 功能
		webview.getSettings().setAppCacheEnabled(false);
		webview.getSettings().setLoadWithOverviewMode(true);
		// webView.getSettings().setDefaultZoom(ZoomDensity.MEDIUM);// 默认缩放模式
		webview.getSettings().setUseWideViewPort(false);
	}

	/*
	 * 获取到服务器数据后开始跟新UI
	 */
	private void upDateUI(String content) {
		// 配置webview数据源
		StringBuilder data = new StringBuilder();
		data.append("<style type=\"text/css\">img{WIDTH:80% !important;HEIGHT:auto !important;}");
		data.append(getFont());
		data.append("</style><base href=\"");
		data.append(HttpInterfaces.ORG);
		data.append("\"/><body class=\"Likun\" style=\"padding-left:2%;padding-right:2%;\">");
		data.append(content);
		data.append("</body>");
		MyLog.e("upDateUI", data.toString());

		// 设置webview 加载数据
		webview.loadDataWithBaseURL(null, data.toString(), "text/html",
				"utf-8", null);
	}

	private String getFont() {
		StringBuilder builder = new StringBuilder();
		// <body class="MsoNormal"> </body>
		builder.append(".Likun { font-family: fzlt;}");
		builder.append("@font-face {font-family:fzlt;");
		builder.append("src:url(file:///android_asset/fzlt.ttf);}");
		return builder.toString();
	}

	@Override
	protected void onResume() {
		super.onResume();
	}

	@Override
	protected void onPause() {
		super.onPause();
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
	}
}
