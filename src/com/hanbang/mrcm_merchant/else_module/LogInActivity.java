package com.hanbang.mrcm_merchant.else_module;

import java.util.ArrayList;
import org.json.JSONException;
import org.json.JSONObject;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import com.hanbang.mrcm_merchant.R;
import com.hanbang.mrcm_merchant.base.BaseActivity;
import com.hanbang.mrcm_merchant.bean.UserInforData;
import com.hanbang.mrcm_merchant.utils.EditHelper;
import com.hanbang.mrcm_merchant.utils.HttpInterfaces;
import com.hanbang.mrcm_merchant.utils.JsonHelper;
import com.hanbang.mrcm_merchant.utils.MyLog;
import com.hanbang.mrcm_merchant.utils.MyRequestCallBack;
import com.hanbang.mrcm_merchant.utils.MySharedPreferences;
import com.hanbang.mrcm_merchant.utils.MyToast;
import com.lidroid.xutils.exception.DbException;
import com.lidroid.xutils.http.RequestParams;
import com.lidroid.xutils.http.ResponseInfo;
import com.lidroid.xutils.http.client.HttpRequest.HttpMethod;

public class LogInActivity extends BaseActivity {

	private EditHelper editHelper = new EditHelper(this);

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.login);
		initView();
		setOnClickListener();
		if (userData != null) {
			((TextView) findViewById(R.id.login_name)).setText(userData
					.getMobile());
			((TextView) findViewById(R.id.login_password)).setText(userData
					.getPassword());
		}
	}

	/*
	 * 设置点击事件
	 */
	private void setOnClickListener() {
		 
		findViewById(R.id.user_login_register_right).setOnClickListener(
				onClickListener);
		findViewById(R.id.user_login_row).setOnClickListener(onClickListener);
		findViewById(R.id.user_login_forget_password).setOnClickListener(
				onClickListener);
	}

	private OnClickListener onClickListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			Intent intent = new Intent();
			switch (v.getId()) {
			case R.id.user_login_register_right:
				intent.setClass(LogInActivity.this, RegisterActivity.class);
				startActivityForResult(intent, 998);
				break;
			case R.id.user_login_row:
				if (editHelper.check()) {
					login();
				}
				break;
			case R.id.user_login_forget_password:
				intent.setClass(LogInActivity.this,
						ForgetPasswordActivity.class);
				startActivity(intent);
				break;
			default:
				break;
			}
		}
	};

	/*
	 * 初始化view
	 */
	private void initView() {
		// 设置顶部的标题
		setBackArrows(findViewById(R.id.top_bar_left_image));
		setTitle(findViewById(R.id.top_bar_title), "用户登录");
		editHelper.setEditText((EditText) findViewById(R.id.login_name),
				(EditText) findViewById(R.id.login_password));
		editHelper.setHintText("请输入用户名", "请输入密码");
		editHelper.setHintMinNumber(1, 1);
	}

	private void login() {
		final ArrayList<String> res = editHelper.getText();
		RequestParams params = new RequestParams();
		params.addBodyParameter("action", "seller_login");
		params.addBodyParameter("user_name", res.get(0));
		params.addBodyParameter("password", res.get(1));
		httpUtils.send(HttpMethod.POST, HttpInterfaces.userLogin, params,
				new MyRequestCallBack<String>(this, "正在登录...") {
					@Override
					public void onSuccess(ResponseInfo<String> responseInfo) {
						super.onSuccess(responseInfo);
						try {
							JSONObject json = new JSONObject(
									responseInfo.result);
							int status = json.getInt("status");
							if (status == 1) {
								UserInforData userInforData = JsonHelper
										.parseObject(json, UserInforData.class);
								try {
									dbUtils.saveOrUpdate(userInforData);
									// 把id 保存在sp
									MySharedPreferences
											.setSharedPreferencesKeyAndValue(
													LogInActivity.this,
													MySharedPreferences.USER_DATA,
													"user_id",
													userInforData.getTrade_id());
									MyLog.e("login", "id保存成功");
									setResult(124);
									finish();
								} catch (DbException e) {
									e.printStackTrace();
									MyLog.e("login", "id保存失败");
									MyToast.show(LogInActivity.this, "登录失败",
											Toast.LENGTH_SHORT);
									MySharedPreferences
											.setSharedPreferencesKeyAndValue(
													LogInActivity.this,
													MySharedPreferences.USER_DATA,
													"user_id", -1);
								}
							} else if (status == -100) {
								passwordIsError(true);
							}
						} catch (JSONException e) {
							e.printStackTrace();
							MyToast.show(LogInActivity.this, "登录失败",
									Toast.LENGTH_LONG);
						}
					}
				});
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if (requestCode == 998 && resultCode == 999) {
			((EditText) findViewById(R.id.login_name)).setText(data
					.getStringExtra("phone"));
			((EditText) findViewById(R.id.login_password)).setText(data
					.getStringExtra("password"));
			login();
		}
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
	}
}
