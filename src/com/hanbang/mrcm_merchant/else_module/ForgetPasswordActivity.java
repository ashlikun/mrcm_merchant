package com.hanbang.mrcm_merchant.else_module;

import org.json.JSONException;
import org.json.JSONObject;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.hanbang.mrcm_merchant.R;
import com.hanbang.mrcm_merchant.base.BaseActivity;
import com.hanbang.mrcm_merchant.utils.HttpInterfaces;
import com.hanbang.mrcm_merchant.utils.MyLog;
import com.hanbang.mrcm_merchant.utils.MyRequestCallBack;
import com.hanbang.mrcm_merchant.utils.MyToast;
import com.lidroid.xutils.http.ResponseInfo;
import com.lidroid.xutils.http.client.HttpRequest.HttpMethod;

public class ForgetPasswordActivity extends BaseActivity {

	// 验证码按钮倒计时 总时长和间隔时长
	private long totalTime = 60000, intervalTime = 1000;
	private TextView authCodeButton;
	private EditText phoneEt;
	private EditText codeEt;
	private EditText passwordEt;
	private EditText password2Et;
	private EditText fp_sellerlogin;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.forget_password);
		initView();
		setOnClickListener();
	}

	/*
	 * 设置点击事件
	 */
	private void setOnClickListener() {
		authCodeButton.setOnClickListener(onClickListener);
		findViewById(R.id.ok).setOnClickListener(onClickListener);

	}

	private OnClickListener onClickListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			switch (v.getId()) {
			case R.id.user_register_auth_code_button:
				sendYZM();
				break;
			case R.id.ok:
				ok();
				break;

			default:
				break;
			}
		}
	};

	/*
	 * 初始化view
	 */
	private void initView() {
		// 设置顶部的标题
		setBackArrows(findViewById(R.id.top_bar_left_image));
		setTitle(findViewById(R.id.top_bar_title), "忘记密码");
		authCodeButton = (TextView) findViewById(R.id.user_register_auth_code_button);
		fp_sellerlogin = (EditText) findViewById(R.id.fp_sellerlogin);
		phoneEt = (EditText) findViewById(R.id.phone);
		codeEt = (EditText) findViewById(R.id.code);
		passwordEt = (EditText) findViewById(R.id.password);
		password2Et = (EditText) findViewById(R.id.password2);

	}

	CountDownTimer countDownTimer = new CountDownTimer(totalTime, intervalTime) {
		@Override
		public void onFinish() {// 计时完毕时触发
			authCodeButton.setText("重新发送");
			authCodeButton.setEnabled(true);
			// authCodeButton.setClickable(true);
		}

		@Override
		public void onTick(long currentTime) {// 计时过程显示
			// authCodeButton.setClickable(false);
			authCodeButton.setEnabled(false);
			authCodeButton.setText(currentTime / 1000 + "秒");
		}
	};

	private void sendYZM() throws IndexOutOfBoundsException {
		final String res = phoneEt.getText().toString().trim();
		if (res == null || res.equals("") || res.length() < 11) {
			MyToast.show(ForgetPasswordActivity.this, "请正确输入手机号！",
					Toast.LENGTH_LONG);
			return;
		}
		MyLog.e("lsgfhosdf", HttpInterfaces.send_verify_sms_code
				+ "action=send_verify_sms_code" + "&mobile=" + res + "&type=1");
		httpUtils.send(HttpMethod.GET, HttpInterfaces.send_verify_sms_code
				  + "&mobile=" + res + "&type=1",
				new MyRequestCallBack<String>(this, "正在发送验证码...") {
					@Override
					public void onSuccess(ResponseInfo<String> responseInfo) {
						super.onSuccess(responseInfo);
						try {
							JSONObject json = new JSONObject(
									responseInfo.result);
							int status = json.getInt("status");
							if (status == 1) {
								countDownTimer.start();
							}
							MyToast.show(ForgetPasswordActivity.this,
									json.getString("msg"), Toast.LENGTH_LONG);
						} catch (JSONException e) {
							e.printStackTrace();
						}
					}
				});
	}

	/*
	 * http://localhost:2642/tools/UserLogin.ashx?action=user_update_pwd&mobile=
	 * 18913114602&code=8687&password=123456&againpassword=123456
	 */
	private void ok() {
		final String res = phoneEt.getText().toString().trim();
		final String res2 = codeEt.getText().toString().trim();
		final String pass1 = passwordEt.getText().toString().trim();
		final String pass2 = password2Et.getText().toString().trim();
		final String fp_sellerlo = fp_sellerlogin.getText().toString().trim();

		if (fp_sellerlo == null || fp_sellerlo.equals("")
				|| fp_sellerlo.length() < 1) {
			MyToast.show(ForgetPasswordActivity.this, "请输入正确的账户名！",
					Toast.LENGTH_LONG);
			return;
		}
		if (res == null || res.equals("") || res.length() < 11) {
			MyToast.show(ForgetPasswordActivity.this, "请正确输入手机号！",
					Toast.LENGTH_LONG);
			return;
		}
		if (res2 == null || res2.equals("") || res2.length() < 1) {
			MyToast.show(ForgetPasswordActivity.this, "请输入验证码",
					Toast.LENGTH_LONG);
			return;
		}
		if (pass1 == null || pass1.equals("") || pass1.length() < 6) {
			MyToast.show(ForgetPasswordActivity.this, "请输入密码",
					Toast.LENGTH_LONG);
			return;
		}
		if (pass2 == null || pass2.equals("") || pass2.length() < 6) {
			MyToast.show(ForgetPasswordActivity.this, "请再次输入密码",
					Toast.LENGTH_LONG);
			return;
		}
		if (!pass1.trim().equals(pass2.trim())) {
			MyToast.show(ForgetPasswordActivity.this, "两次密码不一致",
					Toast.LENGTH_LONG);
			return;
		}

		httpUtils.send(HttpMethod.GET, HttpInterfaces.seller_update_pwd
				+ "&sellerlogin=" + fp_sellerlo + "&password=" + pass1
				+ "&againpassword=" + pass2 + "&mobile=" + res + "&code="
				+ res2, new MyRequestCallBack<String>(this,
				getString(R.string.loadding)) {
			@Override
			public void onSuccess(ResponseInfo<String> responseInfo) {
				super.onSuccess(responseInfo);
				try {
					JSONObject json = new JSONObject(responseInfo.result);
					int status = json.getInt("status");
					MyToast.show(ForgetPasswordActivity.this,
							json.getString("msg"), Toast.LENGTH_LONG);
					if (status == 1) {
						finish();
					}
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}
		});
	}

	@Override
	protected void onResume() {
		super.onResume();
	}

	@Override
	protected void onPause() {
		super.onPause();
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
	}
}
