package com.hanbang.mrcm_merchant.bean;

import com.hanbang.mrcm_merchant.annotation.SelectItem;

public class TimuData {

	/*
	 * "id":1, "title":"青苹果好吃，还是红苹果好吃呀？", "option_a":"还是红苹？青苹果好吃，还是红苹果好吃呀？",
	 * "option_b":"青苹果好吃，还是红苹果好吃青苹果好吃，还是红苹果好吃呀fffffffffffffffffffff？",
	 * "correct_answer":"C"
	 */
	private int id;
	@SelectItem
	private String title;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

}
