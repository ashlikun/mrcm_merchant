package com.hanbang.mrcm_merchant.bean;

public class OldActivityListItemData {
	private String img_url;
	private int past_activity_id;

	public String getImg_url() {
		return img_url;
	}

	public void setImg_url(String img_url) {
		this.img_url = img_url;
	}

	public int getPast_activity_id() {
		return past_activity_id;
	}

	public void setPast_activity_id(int past_activity_id) {
		this.past_activity_id = past_activity_id;
	}

}
