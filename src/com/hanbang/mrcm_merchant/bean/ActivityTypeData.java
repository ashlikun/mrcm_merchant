package com.hanbang.mrcm_merchant.bean;

import com.hanbang.mrcm_merchant.annotation.SelectItem;

public class ActivityTypeData {

	private int id;
	private int type;
	@SelectItem
	private String title;

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

}
