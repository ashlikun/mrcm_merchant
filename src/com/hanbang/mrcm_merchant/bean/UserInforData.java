package com.hanbang.mrcm_merchant.bean;

import com.lidroid.xutils.db.annotation.Id;
import com.lidroid.xutils.db.annotation.NoAutoIncrement;
import com.lidroid.xutils.db.annotation.Table;

@Table(name = "UserInforData")
public class UserInforData {

	/*
	 * "status":1, "msg":"会员登录成功！", "id":"3", "user_name":"yangyu",
	 * "email":"646140788@qq.com",
	 * "avatar":"/upload/201510/19/201510191720412758.jpg", "sex":"保密",
	 * "address":"苏州市沧浪区", "qq":"646140788", "msn":"85858585",
	 * "nickname":"玉xiao", "mobile":"18651196428", "password":"yangyu"
	 */
	@NoAutoIncrement
	@Id(column = "trade_id")
	private int trade_id;
	private String user_name;
	private String email;
	private String avatar;
	private String sex;
	private String address;
	private String qq;
	private String msn;
	private String nickname;
	private String mobile;
	private String password;
	private String msg;
	private String status;

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public int getTrade_id() {
		return trade_id;
	}

	public void setTrade_id(int trade_id) {
		this.trade_id = trade_id;
	}

	public String getUser_name() {
		return user_name;
	}

	public void setUser_name(String user_name) {
		this.user_name = user_name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getAvatar() {
		return avatar;
	}

	public void setAvatar(String avatar) {
		this.avatar = avatar;
	}

	public String getSex() {
		return sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getQq() {
		return qq;
	}

	public void setQq(String qq) {
		this.qq = qq;
	}

	public String getMsn() {
		return msn;
	}

	public void setMsn(String msn) {
		this.msn = msn;
	}

	public String getNickname() {
		return nickname;
	}

	public void setNickname(String nickname) {
		this.nickname = nickname;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

}
