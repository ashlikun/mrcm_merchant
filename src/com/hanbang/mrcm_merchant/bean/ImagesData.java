package com.hanbang.mrcm_merchant.bean;

public class ImagesData {

	/*
	 * "id":258, "article_id":77,
	 * "thumb_path":"/upload/201511/09/thumb_201511091020209319.png",
	 * "original_path":"/upload/201511/09/201511091020209319.png", "type":0
	 */
	private int id;
	private int article_id;
	private String thumb_path;
	private String original_path;
	private String sdPath;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getArticle_id() {
		return article_id;
	}

	public void setArticle_id(int article_id) {
		this.article_id = article_id;
	}

	public String getThumb_path() {
		return thumb_path;
	}

	public void setThumb_path(String thumb_path) {
		this.thumb_path = thumb_path;
	}

	public String getOriginal_path() {
		return original_path;
	}

	public void setOriginal_path(String original_path) {
		this.original_path = original_path;
	}

	public String getSdPath() {
		return sdPath;
	}

	public void setSdPath(String sdPath) {
		this.sdPath = sdPath;
	}

}
