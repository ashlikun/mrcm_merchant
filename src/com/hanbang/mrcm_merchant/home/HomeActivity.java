package com.hanbang.mrcm_merchant.home;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;

import com.hanbang.mrcm_merchant.R;
import com.hanbang.mrcm_merchant.aboutme.AboutMeActivity;
import com.hanbang.mrcm_merchant.base.MainBaseActivity;
import com.hanbang.mrcm_merchant.merchant.MerchantActivity;

public class HomeActivity extends MainBaseActivity {
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.home);
		initView();
		setOnClickListener();
	}

	/*
	 * 设置点击事件
	 */
	private void setOnClickListener() {
		ViewGroup v1 = (ViewGroup) findViewById(R.id.bottom_bar_ll1);
		ViewGroup v2 = (ViewGroup) findViewById(R.id.bottom_bar_ll2);
		ViewGroup v3 = (ViewGroup) findViewById(R.id.bottom_bar_ll3);
		setBottonButton(v1);
		v2.setOnClickListener(onClickListener);
		v3.setOnClickListener(onClickListener);
	}

	private OnClickListener onClickListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			Intent intent = new Intent();
			switch (v.getId()) {
			case R.id.bottom_bar_ll2:
				intent.setClass(HomeActivity.this, MerchantActivity.class);
				intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
				startActivity(intent);
				break;
			case R.id.bottom_bar_ll3:
				intent.setClass(HomeActivity.this, AboutMeActivity.class);
				intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
				startActivity(intent);
				break;
			default:
				break;
			}
		}
	};

	/*
	 * 初始化view
	 */
	private void initView() {
		// 设置顶部的标题
		setTitle(findViewById(R.id.top_bar_title),
				getString(R.string.botton_tab1));

	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
	}
}
